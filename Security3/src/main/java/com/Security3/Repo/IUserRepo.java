package com.Security3.Repo;

import com.Security3.Entity.UserEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserRepo extends JpaRepository<UserEntity, Integer>{
    
    @Query(value = "SELECT * FROM spring_security.user WHERE user_name = :username",nativeQuery = true)
    public UserEntity FindByUserName(@Param("username") String userName);
    
}
