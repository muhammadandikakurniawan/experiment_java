package com.Security3.Repo;

import com.Security3.Entity.RoleEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IRoleRepo extends JpaRepository<RoleEntity,Integer>{
    
}
