package com.Security3;

import com.Security3.Dto.MyUserDetals;
import com.Security3.Dto.UserRoleDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailService implements UserDetailsService {

    @Autowired
    private ProjectService _service;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // return new MyUserDetals(username);
        UserRoleDto data = _service.GetUserRoleByUserName(username);
        return new MyUserDetals(data);
    }
    
}
