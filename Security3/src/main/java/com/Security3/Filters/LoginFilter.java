package com.Security3.Filters;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.Security3.Entity.UserEntity;
import com.Security3.Repo.IUserRepo;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

public class LoginFilter extends GenericFilterBean {

    // @Autowired
    // private AuthenticationManager authManager;

    private IUserRepo _userRepo;

    public LoginFilter(){}

    public LoginFilter(IUserRepo userRepo){
        this._userRepo = userRepo;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest httpReq = (HttpServletRequest)request;
        HttpServletResponse httpRes = (HttpServletResponse)response;

        ObjectMapper objectMapper = new ObjectMapper();

        System.out.println("===================================== LOGIN FILTER ===================================");

        String Username = httpReq.getHeader("username");
        String Password = httpReq.getHeader("password");

        System.out.println(Username);
        System.out.println(Password);

        UserEntity dataUser = this._userRepo.FindByUserName(Username);

        if(dataUser != null){
            System.out.println(dataUser.getEmail());
            System.out.println(dataUser.getName());
            System.out.println(dataUser.getId());
            
            //set custom data principal
            UsernamePasswordAuthenticationToken authReq = new UsernamePasswordAuthenticationToken(Username, Password);
            authReq.setDetails(dataUser);
            
            SecurityContext sc = SecurityContextHolder.getContext();
            sc.setAuthentication(authReq);
            HttpSession session = httpReq.getSession(true);
            session.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, sc);
                    
            chain.doFilter(request, response);
        }else{
            var resBody = new HashMap<String,Object>(){{
                put("message","user is not falid");
            }};
            // response.getWriter().write(objectMapper.writeValueAsString(resBody));
            // response.setContentType("application/json");
            httpRes.setStatus(401);
            httpRes.getWriter().write(objectMapper.writeValueAsString(resBody));
            httpRes.setContentType("application/json");
        }
    }
    
}
