package com.Security3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.Security3.Dto.UserRoleDto;
import com.Security3.Entity.*;
import com.Security3.Repo.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectService {
    @Autowired
	private IRoleRepo _roleRepo;
	@Autowired
    private IUserRepo _userRepo;
    
    public void AddRole(){
		List<RoleEntity> dataRole = new ArrayList();

        List<String> roles = Arrays.asList(new String[]{"user","admin","super"});

        roles.forEach(x -> {
            RoleEntity data = new RoleEntity();
            data.setName(x);
            dataRole.add(data);
        });
		this._roleRepo.saveAll(dataRole);
    }

    public void AddUser(){
        List<UserEntity> dataUser = new ArrayList();
        for(int i = 0; i < 5 ; i++){
            UserEntity data = new UserEntity();
            Integer roleId = i % 2 == 0 ? 2 : 3;
            data.setEmail(String.format("user_%d@gmail.com", i));
            data.setName(String.format("user_%d", i));
            data.setPassword("123456");
            data.setRoleId(roleId);
            dataUser.add(data);
        }
        this._userRepo.saveAll(dataUser);
    }

    public UserRoleDto GetUserRoleByUserName(String username){
        UserEntity dataUser = this._userRepo.FindByUserName(username);
        RoleEntity dataRole = this._roleRepo.findById(dataUser.getRoleId()).get();
        UserRoleDto res = new UserRoleDto();
        res.setRole(dataRole);
        res.setUser(dataUser);
        return res;
    }

    public UserRoleDto GetUserRoleById(Integer id){
        UserEntity dataUser = this._userRepo.findById(id).get();
        RoleEntity dataRole = this._roleRepo.findById(dataUser.getRoleId()).get();
        UserRoleDto res = new UserRoleDto();
        res.setRole(dataRole);
        res.setUser(dataUser);
        return res;
    }
    
}
