package com.Security3.Entity;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.*;

@Entity
@Table(name = "user", schema = "spring_security")
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")   
    @JsonProperty
    private Integer Id;

    @Column(name = "user_name")
    @JsonProperty
    private String Name;   
    
    @Column(name = "user_email")
    @JsonProperty
    private String Email;   

    @Column(name = "user_password")
    @JsonProperty
    private String Password;   

    @Column(name = "role_id")
    @JsonProperty
    private Integer RoleId;
    
    public UserEntity(Integer id, String name, String email, String pass, Integer roleId){
        this.Id = id;
        this.Name = name;
        this.Email = email;
        this.Password = pass;
        this.RoleId = roleId;
    }

    public UserEntity(){

    }
}
