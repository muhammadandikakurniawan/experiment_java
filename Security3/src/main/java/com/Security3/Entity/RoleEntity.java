package com.Security3.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.*;

@Entity
@Table(name = "role", schema = "spring_security")
@Getter
@Setter
// @JsonIgnoreProperties(ignoreUnknown = true)
public class RoleEntity {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    // @JsonProperty
    @Column(name = "role_id")
    private Integer Id;

    @Column(name = "role_name")
    // @JsonProperty
    private String Name;

}
