package com.Security3.Dto;

import com.Security3.Entity.RoleEntity;
import com.Security3.Entity.UserEntity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRoleDto {
    
    private UserEntity User;
    private RoleEntity Role;

}
