package com.Security3.Dto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class MyUserDetals implements UserDetails {

    private String UserName;
    private String Passsword;
    private Collection<? extends GrantedAuthority> Granteds;

    public MyUserDetals(UserRoleDto param){
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< MyUserDetals >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        this.UserName = param.getUser().getName();
        this.Passsword = param.getUser().getPassword();
        System.out.println(this.UserName);
        System.out.println(this.Passsword);
        System.out.println(param.getRole().getName());
        this.Granteds = Arrays.asList(new SimpleGrantedAuthority(String.format("ROLE_%s", param.getRole().getName())));
        System.out.println("=================================================================================");

    }

    public MyUserDetals(String username){
        this.UserName = username;
    }


    public MyUserDetals(){

    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        // penulisan harus "ROLE_[nama role]"
        return this.Granteds;
    }

    @Override
    public String getPassword() {
        return this.Passsword;
    }

    @Override
    public String getUsername() {
        return this.UserName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
    
}
