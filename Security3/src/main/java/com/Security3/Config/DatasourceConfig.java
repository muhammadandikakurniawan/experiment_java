package com.Security3.Config;

import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.*;
import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.*;

@SpringBootConfiguration
@EnableTransactionManagement
@EnableJpaRepositories(
		basePackages = "com.Security3.Repo",
		entityManagerFactoryRef = "TrainingEntityManager", 
		transactionManagerRef = "TrainingTransactionManager"
		)
public class DatasourceConfig {
    @Primary
    @Bean("TrainingDataSource")
    public DataSource TrainingDataSource(){
        DriverManagerDataSource ds = new DriverManagerDataSource();

        ds.setDriverClassName("org.postgresql.Driver");
        ds.setUrl("jdbc:postgresql://localhost:5432/db_training");
        ds.setUsername("postgres");
        ds.setPassword("admin");
        return ds;
    }

    @Primary
    @Bean("TrainingJdbcTemplate")
    public JdbcTemplate TrainingJdbcTemplate(@Qualifier("TrainingDataSource") DataSource ds){
        return new JdbcTemplate(ds);
    }

    @Primary
    @Bean(name="TrainingEntityManager")
    public LocalContainerEntityManagerFactoryBean  TrainingEntityManager(@Qualifier("TrainingDataSource") DataSource ds) {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(ds);
        em.setPackagesToScan("com.Security3.Entity");

        HibernateJpaVendorAdapter vendorAdapter= new HibernateJpaVendorAdapter();
        vendorAdapter.setDatabase(Database.POSTGRESQL);

        em.setJpaVendorAdapter(vendorAdapter);
        em.setPersistenceUnitName("TrainingPU");
        
        HashMap<String, Object> properties = new HashMap<>();
        // properties.put("spring.jpa.hibernate.ddl.auto","update");
        properties.put("hibernate.show_sql",true);
        properties.put("hibernate.dialect","org.hibernate.dialect.PostgreSQLDialect");
        // properties.put("hibernate.hbm2ddl.auto", "create");
        em.setJpaPropertyMap(properties);
 
        return em;
    }
    
    
    @Primary
    @Bean(name="TrainingTransactionManager")
    public PlatformTransactionManager TrainingTransactionManager(@Qualifier("TrainingEntityManager") LocalContainerEntityManagerFactoryBean em) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(em.getObject());
        return transactionManager;
    }
}
