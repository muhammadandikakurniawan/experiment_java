package com.Security3.Config;

import com.Security3.Filters.LoginFilter;
import com.Security3.Repo.IUserRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
    
    @Autowired
    private UserDetailsService _userDetailService;

    @Autowired
    private IUserRepo _userRepo;

    @Autowired
    private AuthenticationManager _authenticationManager;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // super.configure(auth);
        auth.userDetailsService(this._userDetailService);
    }

    @Bean
    public AuthenticationManager customAuthenticationManager() throws Exception {
        return authenticationManager();
    }

    //kalo pake form login
    // @Override
    // protected void configure(HttpSecurity security) throws Exception {
    //     security.authorizeRequests()
    //     .antMatchers("/Admin").hasRole("admin")
    //     .antMatchers("/User").hasRole("user")
    //     .antMatchers("/Home").hasAnyRole("admin","users")
    //     .antMatchers("/Free").permitAll()
    //     .and().formLogin();
    // }

    //kalo pake api
    @Override
    protected void configure(HttpSecurity security) throws Exception {
        security
        .httpBasic()
        .and()
        .addFilterBefore(new LoginFilter(_userRepo), UsernamePasswordAuthenticationFilter.class)
        .authorizeRequests()
        .antMatchers("/Admin").hasAnyRole("admin","super")
        .antMatchers("/User").hasAnyRole("user","super")
        .antMatchers("/Home").hasAnyRole("admin","users","super")
        .antMatchers("/Free").permitAll()
        .and()
        .csrf().disable()
        .formLogin().disable();

        security.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER); //supaya disuruh login terus
    }

    @Bean
    public PasswordEncoder getPasswordEncoder(){
        return NoOpPasswordEncoder.getInstance(); 
    }
}
