package com.Security3.Controllers;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.Security3.MyUserDetailService;
import com.Security3.Entity.UserEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
// @RequestMapping("api/Resource")
public class ResourceController {

    @Autowired
    private MyUserDetailService _userDetail;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private HttpServletResponse response;

    @Autowired
    private AuthenticationManager authManager;

    @RequestMapping(value = "Admin", method = RequestMethod.GET)
    public String Admin() {
        Principal currUser = request.getUserPrincipal();
        return "hello admin " + currUser.getName();
    }

    @RequestMapping(value = "User", method = RequestMethod.GET)
    public String User() {
        Principal currUser = request.getUserPrincipal();
        return "hello user " + currUser.getName();
    }

    @RequestMapping(value = "Free", method = RequestMethod.GET)
    public String Free() {
        Principal currUser = request.getUserPrincipal();
        return "welocome free api user " + currUser.getName();
    }

    @RequestMapping(value = "Home", method = RequestMethod.GET)
    public String Home() {
        Principal currUser = request.getUserPrincipal();
        UserEntity dataUser = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getDetails();
        return "Helcome Home " + currUser.getName() + " " + dataUser.getEmail();
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String Begin() throws UnknownHostException {
        return "Welcome to the HELL";
    }

    @RequestMapping(value = "AutoLogin", method = RequestMethod.GET)
    public void AutoLogin(String Username, String Password) throws IOException {
        
        UsernamePasswordAuthenticationToken authReq = new UsernamePasswordAuthenticationToken(Username, Password);
        Authentication auth = this.authManager.authenticate(authReq);
        SecurityContext sc = SecurityContextHolder.getContext();
        sc.setAuthentication(auth);
        HttpSession session = this.request.getSession(true);
        session.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, sc);

        response.sendRedirect(String.format("%s://%s:%s/Home", request.getScheme(), request.getServerName(),
                request.getServerPort()));
    }

}
