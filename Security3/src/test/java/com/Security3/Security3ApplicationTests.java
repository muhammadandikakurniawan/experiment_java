package com.Security3;

import java.util.*;

import com.Security3.Entity.*;
import com.Security3.Repo.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

@SpringBootTest
class Security3ApplicationTests {

	@Autowired
	private IRoleRepo _roleRepo;
	@Autowired
	private IUserRepo _userRepo;

	@Autowired
	@Qualifier("TrainingJdbcTemplate")
	private JdbcTemplate _jdbcTemplate;

	@Autowired
	private ProjectService _service;


	@Test
	void contextLoads() {
	}


	@Test
	void AddRole() {
		_service.AddRole();
	}

	@Test
	void AddUser() {
		_service.AddUser();
	}

	@Test 
	void addUserCustom(){
		UserEntity data = new UserEntity();
		data.setEmail("dika@gmail.com");
		data.setName("dika");
		data.setPassword("user");
		data.setRoleId(2);
		this._userRepo.save(data);
	}

	@Test
	void getUserRoleByUserName(){
		String userName = "user_1";
		UserEntity dataUser = this._jdbcTemplate.queryForObject(
		"SELECT * FROM spring_security.user WHERE user_name = ?",
		new Object[]{userName},
		(rs,rownNum) -> new UserEntity(
			rs.getInt("user_id"),
			rs.getString("user_name"),
			rs.getString("user_email"),
			rs.getString("user_password"),
			rs.getInt("role_id")
		)
		);

		String dbg = "";
	}

	@Test
	void getUserRoleByUserName2(){
		String userName = "user_1";
		UserEntity dataUser = this._userRepo.FindByUserName(userName);
		RoleEntity dataRole = this._roleRepo.findById(dataUser.getRoleId()).get();
		String dbg = "";
	}

}
