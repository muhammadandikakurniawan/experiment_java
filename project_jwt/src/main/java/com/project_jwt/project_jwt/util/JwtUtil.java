package com.project_jwt.project_jwt.util;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;

@Service
public class JwtUtil {
	
	public JwtBuilder builder(HashMap<String,Object> param,String secretKey) {
		
		HashMap<String,Object> claims = new HashMap<String,Object>();
		String subject="";
		System.out.println("jwtSecretKey = "+secretKey);
		
		try {
			subject = param.get("subject").toString();
			
			if(subject == null) {
				throw new Exception();
			}else {
				for(Map.Entry el : param.entrySet()) {
					if(!el.getKey().toString().equalsIgnoreCase("subject")) {
						claims.put(el.getKey().toString(),param.get(el.getKey()));
					}
				}
			}
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return Jwts.builder()
				.setClaims(claims)
				.setSubject(subject)
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.signWith(SignatureAlgorithm.HS256,secretKey);
	}
	
	public HashMap<String,Object> getClaims(String token,String secretKey) {
		System.out.println("===================== utility ===================");
		HashMap<String,Object> res = new HashMap<String,Object>();
		Boolean isValid = false;
		
		try {
			Claims claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
			isValid = true;
			res.put("claims",claims);
		}
		catch(SignatureException ex) {
			isValid = false;
		}
		catch(Exception ex) {
			isValid = false;
		}
		
		res.put("isValid",isValid);
		System.out.println(token);
		System.out.println(secretKey);
		System.out.println(res);
		return res;
	}
	
	public Boolean isValid(String token,String secretKey) {
		return (Boolean)this.getClaims(token, secretKey).get("isValid");
	}
}
