package com.RabbitMqDemo;

import com.RabbitMqDemo.Services.OrderService;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class RabbitMqDemoApplicationTests {

	@Autowired
	private OrderService _orderServicel;

	@Test
	void contextLoads() {
	}

	@Test
	void AddOrder_Test() {
		_orderServicel.AddOrder();
	}

}
