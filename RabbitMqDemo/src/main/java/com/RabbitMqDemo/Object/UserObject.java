package com.RabbitMqDemo.Object;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.*;

@Getter
@Setter
public class UserObject {

    @JsonProperty("UserId")
    private String UserId;

    @JsonProperty("UserEmail")
    private String UserEmail;

    @JsonProperty("UserName")
    private String UserName;

}
