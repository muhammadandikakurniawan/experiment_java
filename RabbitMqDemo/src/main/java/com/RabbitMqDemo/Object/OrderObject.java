package com.RabbitMqDemo.Object;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.*;

@Getter
@Setter
public class OrderObject {
    
    @JsonProperty("SkuCode")
    public String SkuCode;

    @JsonProperty("Quantity")
    public Integer Quantity;

    @JsonProperty("UserId")
    public String UserId;

    @JsonProperty("TotalPrice")
    public Double TotalPrice;

    public OrderObject(ProductObject p, UserObject u, Integer q){
        this.Product = p;
        this.User = u;
        this.Quantity = q;
        this.SkuCode = p.getSkuCode();
        this.UserId = u.getUserId();
        this.TotalPrice = p.getPrice() * q;
    }

    @JsonProperty("Product")
    private ProductObject Product;

    @JsonProperty("User")
    private UserObject User;

}
