package com.RabbitMqDemo.Object;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.*;

@Getter
@Setter
public class ProductObject {
   
    @JsonProperty("SkuCode")
    private String SkuCode;

    @JsonProperty("Name")
    private String Name;

    @JsonProperty("Price")
    private Double Price;

}
