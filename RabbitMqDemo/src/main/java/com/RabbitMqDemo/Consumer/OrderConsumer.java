package com.RabbitMqDemo.Consumer;

import java.lang.reflect.Field;

import com.RabbitMqDemo.Config.RabbitMqConfig;
import com.RabbitMqDemo.Object.OrderObject;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderConsumer {
    
    @Autowired
    private ObjectMapper _objMapper;

    @RabbitListener(queues = RabbitMqConfig.Queue1)
    public void AddOrder(Object data) throws IllegalArgumentException, IllegalAccessException {
        // for(Field f : data.getClass().getDeclaredFields()){
        //     var values = f.get(data);
        //     System.out.println(values);
        // }
        System.out.println(data);
    }

    @RabbitListener(queues = RabbitMqConfig.Queue2)
    public void AddOrder2(Object data) throws IllegalArgumentException, IllegalAccessException {
        for(Field f : data.getClass().getDeclaredFields()){
            var values = f.get(data);
            System.out.println(values);
        }
    }

}
