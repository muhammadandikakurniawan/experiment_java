package com.RabbitMqDemo.Config;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.Jackson2XmlMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
@EnableRabbit
public class RabbitMqConfig {
    
    public final static String Queue1 = "spring_queue1";
    public final static String Queue2 = "spring_queue2";

    public static String Exchange1 = "spring_exchange1";
    public static String Exchange2 = "spring_exchange2";

    public static String RouteKey1 = "spring_route_key1";
    public static String RouteKey2 = "spring_route_key2";

    @Bean("queue1")
    public Queue queue1(){
        return new Queue(Queue1);
    }

    @Bean("queue2")
    public Queue queue2(){
        return new Queue(Queue2);
    }

    @Bean("exchange1")
    public TopicExchange exchange1(){
        return new TopicExchange(Exchange1);
    }

    @Bean("exchange2")
    public TopicExchange exchange2(){
        return new TopicExchange(Exchange2);
    }

    @Bean("bind1")
    public Binding bind1(@Qualifier("queue1") Queue q, @Qualifier("exchange1") TopicExchange ex){
        return BindingBuilder.bind(q).to(ex).with(RouteKey1);
    }

    //test multiple route key in 1 exchange, dengan exhange routing key yang sama, namun queue yg beda
    @Bean("bind2")
    public Binding bind2(@Qualifier("queue1") Queue q, @Qualifier("exchange2") TopicExchange ex){
        return BindingBuilder.bind(q).to(ex).with(RouteKey2);
    }

    @Bean("bind3")
    public Binding bind3(@Qualifier("queue2") Queue q, @Qualifier("exchange2") TopicExchange ex){
        return BindingBuilder.bind(q).to(ex).with(RouteKey2);
    }

    @Bean
    public MessageConverter converter(){
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory){
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(converter());
        return rabbitTemplate;
    }
}
