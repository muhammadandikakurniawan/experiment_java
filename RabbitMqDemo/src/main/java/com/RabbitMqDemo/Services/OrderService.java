package com.RabbitMqDemo.Services;

import java.util.UUID;

import com.RabbitMqDemo.Config.RabbitMqConfig;
import com.RabbitMqDemo.Object.*;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderService {

    @Autowired
    private ObjectMapper _objectMapper;

    @Autowired
    private RabbitTemplate _rabbitTemplate;
    
    public Boolean AddOrder(){
      Boolean res = true;

      try{

        ProductObject dataProduct = new ProductObject(){{
            setSkuCode(String.format("PRD-%s", UUID.randomUUID().toString()));
            setName("Bebe soya");
            setPrice(100.000);
        }};

        UserObject datUser = new UserObject(){{
            setUserId(String.format("USR-%s", UUID.randomUUID().toString()));
            setUserEmail(String.format("%s@gmail.com", UUID.randomUUID().toString()));
            setUserName("Andika");
        }};

        OrderObject dataOrder = new OrderObject(dataProduct, datUser, 10);

        //send data lebih spesifik dengan exchange dan routing key
        // this._rabbitTemplate.convertAndSend(RabbitMqConfig.Exchange1, RabbitMqConfig.RouteKey1, dataOrder);

        //bisa kirim pake routing key aja
        this._rabbitTemplate.convertAndSend(RabbitMqConfig.Exchange2, RabbitMqConfig.RouteKey2, dataOrder);


      }catch(Exception ex){
          String errMsg = ex.getMessage();
          res = false;
      }

      return res;
    }

}
