package com.javasecurity.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.javasecurity.model.RoleModel;
import com.javasecurity.model.UserModel;

@Service
public class RoleDao {

	@Autowired
	@Qualifier("databaseJbcTemplate")
	private JdbcTemplate jdbcT;
	
	private String query = "";
	
	@Transactional(rollbackFor = {Exception.class},propagation=Propagation.REQUIRED)
	public HashMap<String,Object> createRole(RoleModel params){
		HashMap<String,Object> res = new HashMap<String,Object>();
		ObjectMapper objMapper = new ObjectMapper();
		try {
			query = String.format("insert into auth.tb_role (role_name) values(%s)", "?");
			Integer add =  this.jdbcT.update(query, new PreparedStatementSetter() {

				@Override
				public void setValues(PreparedStatement ps) throws SQLException {
					ps.setString(1, params.getRole_name());
				}
				
			});
			if(add > 0) {
				res.put("status","success");
			}else {
				res.put("status","failed");
			}
		}catch(Exception ex) {
			res.put("status","error");
			res.put("error_msg",ex.getMessage());
		}
		
		return res;
	}  
	
	@Transactional(rollbackFor = {Exception.class},propagation=Propagation.REQUIRED)
	public RoleModel getRoleById(Integer param){
		RoleModel res = null;
		ObjectMapper objMapper = new ObjectMapper();
		try {
			HashMap<String,Object> queryParams = new HashMap<String,Object>(){
				{
					put("id",param);
				}
			};
			query = String.format("select * from auth.tb_role where role_id = %s", "?");
			res = this.jdbcT.queryForObject(query,new Object[] {param},new RowMapper<RoleModel>() {

				@Override
				public RoleModel mapRow(ResultSet rs, int rowNum) throws SQLException {
					RoleModel roleModel = new RoleModel();
					roleModel.setRole_id(rs.getInt("role_id"));
					roleModel.setRole_name(rs.getString("role_name"));
					return roleModel;
				}
				
			}
			);
			
		}catch(Exception ex) {
			res = null;
			ex.printStackTrace();
		}
		
		return res;
	}
	
}
