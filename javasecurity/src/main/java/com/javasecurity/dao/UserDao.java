package com.javasecurity.dao;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.javasecurity.dto.UserRoleDto;
import com.javasecurity.model.RoleModel;
import com.javasecurity.model.UserModel;
import com.javasecurity.repo.UserRepo;

@Service
public class UserDao {

	@Autowired
	@Qualifier("databaseJbcTemplate")
	private JdbcTemplate jdbcT;
	
	@Autowired
	private UserRepo userRepo;
	
	@Autowired
	private RoleDao roleDao;
	
	private String query = "";
	
	public List<UserModel> getUserData(){
		return this.userRepo.findAll();
	}
	
	@Transactional(rollbackFor = {Exception.class},propagation=Propagation.REQUIRED)
	public HashMap<String,Object> createUser(UserModel params){
		HashMap<String,Object> res = new HashMap<String,Object>();
		
		try {
			
			if(this.roleDao.getRoleById(params.getUser_role()) != null) {
				if(this.userRepo.save(params) != null) {
					res.put("status","success");
				}else {
					res.put("status","failed");
				}
			}else {
				res.put("status","failed");
				res.put("msg","role is not valid");
//				TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
//				TransactionInterceptor.currentTransactionStatus().flush();
			}
			
		}catch(Exception ex) {
			res.put("status","error");
			res.put("error_msg",ex.getMessage());
		}
		
		return res;
	} 
	
	@Transactional(rollbackFor = {Exception.class},propagation=Propagation.REQUIRED)
	public List<UserRoleDto> getUserAndRole(){
		List<UserRoleDto> res = new ArrayList<UserRoleDto>();
		ObjectMapper objMapper = new ObjectMapper();
		try {
			this.query = String.format("SELECT usr.*,rl.* FROM auth.tb_user usr join auth.tb_role rl on rl.role_id = usr.user_role ");
			res = this.jdbcT.query(query,new RowMapper<UserRoleDto>() {

				@Override
				public UserRoleDto mapRow(ResultSet rs, int rowNum) throws SQLException {
					System.out.println("===================== ROW MAPPER ========================");
					
					ResultSetMetaData rsmd = rs.getMetaData();
					
						try {
							System.out.println(objMapper.writeValueAsString(rs.getObject(1)));
						} catch (JsonProcessingException e) {
							e.printStackTrace();
						}
					for(Integer i = 1; i <= rsmd.getColumnCount(); i++) {
						System.out.println(rsmd.getColumnName(i));
					}
					UserModel user = new UserModel(
							rs.getString(1),
							rs.getString(2),
							rs.getString(3),
							rs.getInt(4),
							rs.getString(5));
					
					RoleModel role = new RoleModel(rs.getInt(6),rs.getString(7));
					
					UserRoleDto userRole = new UserRoleDto(user,role);
					
					return userRole;
				}
				
				
				
			});
			
			System.out.println("===================== result ========================");
			System.out.println(res);
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return res;
	} 
	
	@Transactional(rollbackFor = {Exception.class},propagation=Propagation.REQUIRED)
	public UserRoleDto getUserAndRoleByUsername(String param){
		UserRoleDto res = new UserRoleDto();
		ObjectMapper objMapper = new ObjectMapper();
		try {
			this.query = String.format("SELECT usr.*,rl.* FROM auth.tb_user usr join auth.tb_role rl on rl.role_id = usr.user_role where usr.user_name = "+param);
			res = (UserRoleDto) this.jdbcT.query(query,new RowMapper<UserRoleDto>() {

				@Override
				public UserRoleDto mapRow(ResultSet rs, int rowNum) throws SQLException {
					System.out.println("===================== ROW MAPPER ========================");
					
					ResultSetMetaData rsmd = rs.getMetaData();
					
						try {
							System.out.println(objMapper.writeValueAsString(rs.getObject(1)));
						} catch (JsonProcessingException e) {
							e.printStackTrace();
						}
					for(Integer i = 1; i <= rsmd.getColumnCount(); i++) {
						System.out.println(rsmd.getColumnName(i));
					}
					UserModel user = new UserModel(
							rs.getString(1),
							rs.getString(2),
							rs.getString(3),
							rs.getInt(4),
							rs.getString(5));
					
					RoleModel role = new RoleModel(rs.getInt(6),rs.getString(7));
					
					UserRoleDto userRole = new UserRoleDto(user,role);
					
					return userRole;
				}
				
				
				
			});
			
			System.out.println("===================== result ========================");
			System.out.println(res);
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return res;
	} 
	
	public List<Map<String, Object>> joinUserRole(){
		return this.userRepo.joinUserRole();
	}
	
}
