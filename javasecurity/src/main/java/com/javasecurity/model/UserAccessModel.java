package com.javasecurity.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_user_access",schema="auth")
public class UserAccessModel {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer acces_id;
	
	private String access_point;
	
	private Integer role_id;

	public Integer getRole_id() {
		return role_id;
	}

	public void setRole_id(Integer role_id) {
		this.role_id = role_id;
	}

	public Integer getAcces_id() {
		return acces_id;
	}

	public void setAcces_id(Integer acces_id) {
		this.acces_id = acces_id;
	}

	public String getAccess_point() {
		return access_point;
	}

	public void setAccess_point(String access_point) {
		this.access_point = access_point;
	}
	
	
	
}
