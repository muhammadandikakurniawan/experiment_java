package com.javasecurity.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

import javax.persistence.JoinColumn;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.javasecurity.dto.UserDto;

@Entity
@Table(name="tb_user",schema="auth")
public class UserModel {

	@Id
	private String user_id;
	
	private String user_name;
	private String user_password;
	private Integer user_role;
	private String user_token;

	public UserModel() {}
	
	public UserModel(String id, String name, String pass, Integer role) {
		this.setUser_id(id);
		this.setUser_name(name);
		this.setUser_password(pass);
		this.setUser_role(role);
	}
	public UserModel(String id, String name, String pass, Integer role,String token) {
		this.setUser_id(id);
		this.setUser_name(name);
		this.setUser_password(pass);
		this.setUser_role(role);
		this.setUser_token(token);
	}
	public UserModel(UserDto params) {
		this.setUser_name(params.getUser_name());
		this.setUser_password(params.getUser_password());
		this.setUser_role(params.getUser_role());
	}
	public UserModel(String id,UserDto params) {
		this.setUser_id(id);
		this.setUser_name(params.getUser_name());
		this.setUser_password(params.getUser_password());
		this.setUser_role(params.getUser_role());
	}
	public UserModel(String id, String name, String pass) {
		this.setUser_id(id);
		this.setUser_name(name);
		this.setUser_password(pass);
	}
	
	@JsonProperty("id")
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	
	@JsonProperty("name")
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	
	@JsonProperty("password")
	public String getUser_password() {
		return user_password;
	}
	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}
	
	@JsonProperty("role")
	public Integer getUser_role() {
		return user_role;
	}
	public void setUser_role(Integer user_role) {
		this.user_role = user_role;
	}
	
	@JsonProperty("token")
	public String getUser_token() {
		return user_token;
	}

	public void setUser_token(String user_token) {
		this.user_token = user_token;
	}
	
	
}
