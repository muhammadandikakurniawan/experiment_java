package com.javasecurity.filter;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.javasecurity.securityItem.ProjectUserDetailService;
import com.javasecurity.securityItem.ProjectUserDetails;
import com.javasecurity.util.HttpUtil;

@WebFilter(urlPatterns= {"/menu/*","/test/*"})
@Component
public class AuthFilter implements Filter{

	@Autowired
	private HttpUtil httpUtil;
	
    @Autowired
    @Qualifier("ProjectUserDetailService")
    private ProjectUserDetailService userDetailsService;

	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpReq = (HttpServletRequest) request;
		HttpServletResponse httpRes = (HttpServletResponse) response;
		ObjectMapper objMapper = new ObjectMapper();
		ProjectUserDetails usrDetail = new ProjectUserDetails();
		usrDetail.setData("data user login");
//		HashMap<String,Object> requestParam = this.httpUtil.getRequestParam(httpReq);
		System.out.println("=================================== FILTER ================================");
		System.out.println("================================ REQUEST PARAM ============================");
//		System.out.println(objMapper.writeValueAsString(requestParam));
		System.out.println("===========================================================================");
		chain.doFilter(httpReq, response);
	}

}
