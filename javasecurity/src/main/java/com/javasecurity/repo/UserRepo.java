package com.javasecurity.repo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.javasecurity.model.UserModel;

@Repository
public interface UserRepo extends JpaRepository<UserModel,String>{

	@Query(value="SELECT u.*,r.* FROM auth.tb_user u JOIN auth.tb_role r ON u.user_role = r.role_id",nativeQuery=true)
	public List<Map<String, Object>> joinUserRole();
	
}
