package com.javasecurity.dto;

import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserDto {
	private String user_name;
	private String user_password;
	private Integer user_role;
	
	public UserDto() {}
	
	public UserDto(String name, String pass, Integer role) {
		this.setUser_name(name);
		this.setUser_password(pass);
		this.setUser_role(role);
	}
	public UserDto(String id, String name, String pass) {
		this.setUser_name(name);
		this.setUser_password(pass);
	}
	
	@JsonProperty("name")
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	
	@JsonProperty("password")
	public String getUser_password() {
		return user_password;
	}
	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}
	
	@JsonProperty("role")
	public Integer getUser_role() {
		return user_role;
	}
	public void setUser_role(Integer user_role) {
		this.user_role = user_role;
	}
}
