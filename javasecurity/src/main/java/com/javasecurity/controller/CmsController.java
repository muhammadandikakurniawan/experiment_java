package com.javasecurity.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.javasecurity.dto.UserDto;
import com.javasecurity.dto.UserRoleDto;
import com.javasecurity.model.RoleModel;
import com.javasecurity.model.UserModel;
import com.javasecurity.service.CmsService;

@RestController
@RequestMapping("/cms")
public class CmsController {

	@Autowired
	private CmsService cmsService;
	
	@RequestMapping(value="/createuser",method=RequestMethod.POST)
	public HashMap<String,Object> createUser(@RequestBody UserDto params) throws JsonProcessingException{
		HashMap<String,Object> res = new HashMap<String,Object>();
		ObjectMapper objMapper = new ObjectMapper();
		System.out.println(objMapper.writeValueAsString(params));
		try {
			res = this.cmsService.createUser(params);
		}catch(Exception ex) {
			res.put("status","error");
			res.put("error_msg",ex.getMessage());
		}
		
		return res;
	}
	
	@RequestMapping(value="/createrole",method=RequestMethod.POST)
	public HashMap<String,Object> createRole(@RequestBody RoleModel params){
		HashMap<String,Object> res = new HashMap<String,Object>();
			
		try {
			res = this.cmsService.createRole(params);
		}catch(Exception ex) {
			res.put("status","error");
			res.put("error_msg",ex.getMessage());
		}
		
		return res;
	}
	
	@RequestMapping(value="/getUserAndRole",method=RequestMethod.POST)
	public List<UserRoleDto> getUserAndRole(){
		System.out.print(this.cmsService.getUserAndRole());
		return this.cmsService.getUserAndRole();
	}
	
	@RequestMapping(value="/getUserAndRole2",method=RequestMethod.POST)
	public List<Map<String, Object>> getUserAndRole2(){
		System.out.print(this.cmsService.findUsers());
		return this.cmsService.findUsers();
	}
	
}
