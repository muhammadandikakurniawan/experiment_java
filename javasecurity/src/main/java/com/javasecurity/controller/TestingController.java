package com.javasecurity.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.javasecurity.service.Service1;

@RestController
@RequestMapping("/test")
public class TestingController {

	@Autowired
	private Service1 service1;
	
	@RequestMapping(value="/userrole",method={RequestMethod.POST,RequestMethod.GET})
	public HashMap<String,Object> userRole(){
		HashMap<String,Object> res = new HashMap<String,Object>();
		
		try {
			res.put("status",HttpStatus.OK);
		}catch(Exception ex) {
			res.put("status",HttpStatus.INTERNAL_SERVER_ERROR);
			res.put("error_message",ex.getMessage());
		}
		
		return res;
	}
	
	@RequestMapping(value="/testparam",method=RequestMethod.POST)
	public HashMap<String,Object> testParam(@RequestBody() HashMap<String,Object> param){
		HashMap<String,Object> res = new HashMap<String,Object>();
		
		try {
			res.put("status",HttpStatus.OK);
		}catch(Exception ex) {
			res.put("status",HttpStatus.INTERNAL_SERVER_ERROR);
			res.put("error_message",ex.getMessage());
		}
		
		return res;
	}
	
}
