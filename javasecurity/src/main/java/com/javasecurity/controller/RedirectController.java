package com.javasecurity.controller;

import java.util.HashMap;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/redirect")
public class RedirectController {

	@RequestMapping(value="/forbiden")
	public HashMap<String,Object> forbid(){
		HashMap<String,Object> res = new HashMap<String,Object>();
		
		res.put("message", "access forbidden for your role");
		res.put("code",HttpStatus.FORBIDDEN);
		
		return res;
	}
	
}
