package com.javasecurity.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/menu")
public class MenuController {

	@RequestMapping(value="/admin")
//	@ResponseBody
//	@RequestBody HashMap<String,Object> param,HttpServletResponse httpRes
	public HashMap<String,Object> admin(Authentication auth){
		HashMap<String,Object> res = new HashMap<String,Object>();
		System.out.println("===================================== PRINCIPAL ====================================");
		System.out.println(auth.getPrincipal());
		res.put("status","ok");
		res.put("username",auth.getName());
		res.put("code",HttpStatus.OK);
		return res;
	}
	
	@RequestMapping(value="/superuser")
//	@ResponseBody
//	@RequestBody HashMap<String,Object> param,HttpServletResponse httpRes
	public HashMap<String,Object> superUser(Authentication auth){
		HashMap<String,Object> res = new HashMap<String,Object>();
		System.out.println("===================================== PRINCIPAL ====================================");
		System.out.println(auth.getPrincipal());
		res.put("status","ok");
		res.put("username",auth.getName());
		res.put("code",HttpStatus.OK);
		return res;
	}
	
}
