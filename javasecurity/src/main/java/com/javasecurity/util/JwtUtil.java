package com.javasecurity.util;

import java.util.Date;
import java.util.HashMap;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;

@Service
public class JwtUtil {
	
	
	public JwtBuilder getBuilder(HashMap<String,Object> params,String secretKey,SignatureAlgorithm signAlgo) throws JsonProcessingException {
		HashMap<String,Object> payload = new HashMap<>();
		JwtBuilder jwtBuilder = null;
		ObjectMapper objMapper = new ObjectMapper();
		System.out.println(objMapper.writeValueAsString(params));
		System.out.println("secret key = "+secretKey);
		try {
			
			String subject = params.get("subject") != null ? params.get("subject").toString() : "";
			
			if(subject.equals("")) {
				System.out.println("subject = null");
				throw new Exception();
			}else {
				System.out.println("create builder");
				params.remove("subject");
				jwtBuilder = Jwts.builder()
				.setClaims(params)
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setSubject(subject)
				.signWith(signAlgo, secretKey);
				
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		
		
		return jwtBuilder;
	}
	
	public HashMap<String,Object> jws(String token, String secretKey){
		HashMap<String,Object> res = new HashMap<String,Object>();
		res.put("isValid", true);
		res.put("isExpired", false);
		res.put("claims", null);
		
		try {
			Jws jws = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
			res.replace("claims", jws);
		}
		catch(ExpiredJwtException ex) {
			res.replace("isExpired", true);
			res.replace("isValid", false);
		}
		catch(SignatureException ex) {
			res.replace("isValid", false);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return res;
	}
	
	public Boolean isValid(String token, String secretKey) {
		return (Boolean) this.jws(token, secretKey).get("isValid");
	}
	
	public Boolean isExpired(String token, String secretKey) {
		return (Boolean) this.jws(token, secretKey).get("isExpired");
	}
	
	public Claims getClaims(String token, String secretKey) {
		return (Claims) this.jws(token, secretKey).get("claims");
	}
	
}
