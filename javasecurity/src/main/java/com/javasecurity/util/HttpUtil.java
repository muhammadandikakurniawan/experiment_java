package com.javasecurity.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class HttpUtil {
	
	public HashMap<String,Object> getRequestParam(HttpServletRequest param) throws IOException{
		
		ObjectMapper objMapper = new ObjectMapper();
		
		HashMap<String,Object> result = new HashMap<String,Object>();
		
		HttpServletRequestWrapper reqContentWrapper = new HttpServletRequestWrapper(param);
		BufferedReader bufferReader = new BufferedReader(new InputStreamReader(reqContentWrapper.getInputStream()));
		char[] charBuffer = new char[128];
		int byteRead = -1;
		StringBuilder strBuilder = new StringBuilder();
		
		while((byteRead = bufferReader.read(charBuffer)) != -1) {
			strBuilder.append(charBuffer,0,byteRead);
		}
		
		result = objMapper.readValue(strBuilder.toString(),HashMap.class);
			
		return result;
		
	}
	
}
