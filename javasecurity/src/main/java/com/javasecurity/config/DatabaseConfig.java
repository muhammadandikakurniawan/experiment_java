package com.javasecurity.config;

import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootConfiguration
@EnableTransactionManagement
@EnableJpaRepositories(
			basePackages= {"com.javasecurity.repo"},
			entityManagerFactoryRef="databaseEntityManager",
			transactionManagerRef="databaseTransactionManager"
		)
public class DatabaseConfig {
	
	@Primary
	@Bean("databaseConnectionFactory")
	public DataSource ds() {
		DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setUrl("jdbc:postgresql://localhost:5432/latihan");
		ds.setPassword("humorismenalws06");
		ds.setUsername("andi");
		ds.setDriverClassName("org.postgresql.Driver");
		
		return ds;	
	}
	
	@Primary
    @Bean(name="databaseEntityManager")
    public LocalContainerEntityManagerFactoryBean dbEntityManager(@Qualifier("databaseConnectionFactory") DataSource ds) {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(ds);
        em.setPackagesToScan(new String[] { "com.javasecurity.model" });
 
        HibernateJpaVendorAdapter vendorAdapter= new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setPersistenceUnitName("databaseEntityManager");
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto","update");
        properties.put("hibernate.dialect","org.hibernate.dialect.PostgreSQLDialect");
        em.setJpaPropertyMap(properties);
        return em;
    }
	
	@Primary
    @Bean(name="databaseTransactionManager")
    public PlatformTransactionManager dbTransactionManager(@Qualifier("databaseEntityManager") LocalContainerEntityManagerFactoryBean em) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(em.getObject());
        return transactionManager;
    }
	
	@Primary
	@Bean("databaseJbcTemplate")
	public JdbcTemplate jdbcT(@Qualifier("databaseConnectionFactory") DataSource ds) {
		return new JdbcTemplate(ds);
	}
	
	
	
	
}
