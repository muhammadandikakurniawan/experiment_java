package com.javasecurity.config;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.javasecurity.dao.UserDao;
import com.javasecurity.dto.UserRoleDto;

@SpringBootConfiguration
@EnableWebSecurity
public class JavaSecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Autowired
	private UserDao userDao;
	
	private ObjectMapper objMapper = new ObjectMapper();
	
	@Override
	public void configure(AuthenticationManagerBuilder authBuilder) throws Exception {
		
		System.out.println("============================ AUTH CONFIGURATION ================================");
		
		System.out.println(this.objMapper.writeValueAsString(this.userDao.getUserAndRole()));
		
		for(UserRoleDto userRole : this.userDao.getUserAndRole()) {
			authBuilder
			.inMemoryAuthentication()
			.withUser(userRole.getUserModel().getUser_name()).
			password(this.passwordEncoder().encode(userRole.getUserModel().getUser_password())).
			roles(userRole.getRoleModel().getRole_name());
		}
		
	}
	

	@Override
	protected void configure(HttpSecurity httpsecurity) throws Exception {
		httpsecurity
		.csrf().disable()
		.authorizeRequests()
		.antMatchers("/cms/**").permitAll()
		.antMatchers("/test/**").permitAll()
		.antMatchers("/menu/admin").hasAnyRole("admin","super")
		.antMatchers("/menu/superuser").hasRole("super")
		.and()
		.httpBasic();
//		.authorizeRequests()
//		.anyRequest().authenticated()
//		.and()
//		.httpBasic();
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
//	@Bean
//	@Override
//	public UserDetailsService userDetailsService() {
//		UserDetails user = User.withDefaultPasswordEncoder()
//		.username("user")
//		.password("password123")
//		.roles("USER")
//		.build();
//
//		return new InMemoryUserDetailsManager(user);
//	}
	
}




















//public class JavaSecurityConfig{
//	
//	
////	 private String id = UUID.randomUUID().toString();
////	@Override
//	
//}