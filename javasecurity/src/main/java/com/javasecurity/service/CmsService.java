package com.javasecurity.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.javasecurity.dao.RoleDao;
import com.javasecurity.dao.UserDao;
import com.javasecurity.dto.UserDto;
import com.javasecurity.dto.UserRoleDto;
import com.javasecurity.model.RoleModel;
import com.javasecurity.model.UserModel;
import com.javasecurity.util.JwtUtil;

import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class CmsService {

	@Autowired
	private UserDao userDao;
	
	@Autowired
	private RoleDao roleDao;
	
	@Autowired
	private JwtUtil jwtUtil;
	
	public HashMap<String,Object> createUser(UserDto params){
		HashMap<String,Object> res = new HashMap<String,Object>();
		ObjectMapper objMapper = new ObjectMapper();
		UserModel dataUser = new UserModel(params);
		
		try {
			dataUser.setUser_id(String.format("USER-%s", UUID.randomUUID().toString().substring(0,5)));
			
			HashMap<String,Object> payloadJwt = new HashMap<String,Object>();
			payloadJwt = objMapper.convertValue(dataUser, HashMap.class);
			payloadJwt.put("subject", params.getUser_name());
			payloadJwt.remove("token");
			
			System.out.println(objMapper.writeValueAsString(payloadJwt));
			
			dataUser.setUser_token(this.jwtUtil.getBuilder(payloadJwt, params.getUser_password(), SignatureAlgorithm.HS256).compact());
			
			res = this.userDao.createUser(dataUser);
		}catch(Exception ex) {
			res.put("status","error");
			res.put("error_msg",ex.getMessage());
		}
		
		return res;
	}
	
	public HashMap<String,Object> createRole(RoleModel params){
		HashMap<String,Object> res = new HashMap<String,Object>();
		
		try {
			res = this.roleDao.createRole(params);
		}catch(Exception ex) {
			res.put("status","error");
			res.put("error_msg",ex.getMessage());
		}
		
		return res;
	}
	
	
	public List<UserRoleDto> getUserAndRole(){	
		return this.userDao.getUserAndRole();
	}
	
	public List<Map<String, Object>> findUsers(){	
		return this.userDao.joinUserRole();
	}
	
}
