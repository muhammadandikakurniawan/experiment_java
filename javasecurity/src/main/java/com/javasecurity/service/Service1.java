package com.javasecurity.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class Service1 {
	
	@Autowired
	@Qualifier("databaseJbcTemplate")
	private JdbcTemplate jdbcT;

}
