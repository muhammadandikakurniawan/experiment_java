package free.project.dto;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SignatureDTO {
	private String stringByte;
	private String location;
	private Timestamp date;
	
	public String getStringByte() {
		return stringByte;
	}
	
	@JsonProperty(value="string_byte")
	public void setStringByte(String stringByte) {
		this.stringByte = stringByte;
	}
	public String getLocation() {
		return location;
	}
	
	@JsonProperty(value="location")
	public void setLocation(String location) {
		this.location = location;
	}
	public Timestamp getDate() {
		return date;
	}
	
	@JsonProperty(value="date")
	public void setDate(Timestamp date) {
		this.date = date;
	}
}
