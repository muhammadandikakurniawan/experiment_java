package free.project.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SignServiceDTO {

	private String agenCode;
	private String otpCode;
	private SignatureDTO signature;
	private String clientCode;
	
	public String getClientCode() {
		return clientCode;
	}

	@JsonProperty(value="client_code")
	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}

	public String getAgenCode() {
		return agenCode;
	}
	
	@JsonProperty(value="agen_code")
	public void setAgenCode(String agenCode) {
		this.agenCode = agenCode;
	}

	public String getOtpCode() {
		return otpCode;
	}
	@JsonProperty(value="otp_code")
	public void setOtpCode(String otpCode) {
		this.otpCode = otpCode;
	}
	
	public SignatureDTO getSignature() {
		return signature;
	}
	@JsonProperty(value="signature")
	public void setSignature(SignatureDTO signature) {
		this.signature = signature;
	}
	
}
