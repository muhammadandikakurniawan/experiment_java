package free.project.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GenerateCodeDTO {
	private String agenCode;
	private String clientCode;
	private String otpCode;
	
	public String getOtpCode() {
		return otpCode;
	}

	@JsonProperty("otp_code")
	public GenerateCodeDTO setOtpCode(String otpCode) {
		this.otpCode = otpCode;
		return this;
	}

	public String getAgenCode() {
		return agenCode;
	}
	
	@JsonProperty("agen_code")
	public GenerateCodeDTO setAgenCode(String agenCode) {
		this.agenCode = agenCode;
		return this;
	}
	public String getClientCode() {
		return clientCode;
	}
	
	@JsonProperty("client_code")
	public GenerateCodeDTO setClientCode(String clientCode) {
		this.clientCode = clientCode;
		return this;
	}
	
	
}
