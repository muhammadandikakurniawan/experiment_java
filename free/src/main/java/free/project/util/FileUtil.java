package free.project.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Service;

@Service
public class FileUtil {
	
	public Boolean convertBase64ToFile(String base64FileParam,String fileName) {
		Boolean res = false;
		ClassLoader classLoader = getClass().getClassLoader();
		System.out.println("====================== FILE UTIL ====================");
//		System.out.println(base64FileParam);
		try {
			
			byte[] byteBase64File = Base64.decodeBase64(base64FileParam);
			System.out.println("====================== FILE EXTENTION ====================");
			
			FileOutputStream fout = new FileOutputStream(fileName);
			
			fout.write(byteBase64File);
			fout.close();
			
			res = this.isExistSign(fileName);
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return res;
	}
	
	public String getExtentionFileBase64(String param) throws IOException {
		byte[] byteBase64File = Base64.decodeBase64(param);
		InputStream inputStrm = new ByteArrayInputStream(byteBase64File);
		
		String exFile = URLConnection.guessContentTypeFromStream(inputStrm).split("/")[1];
		
		return exFile;
	}
	
	public Boolean isExistSign(String param) {
		Boolean res = false;
			try {
				res = new File(param).exists();
			}catch(Exception ex) {
				ex.printStackTrace();
			}
		return res;
	}
	
}
