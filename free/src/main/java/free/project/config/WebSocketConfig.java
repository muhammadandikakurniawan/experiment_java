package free.project.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer{
	
	@Override
	public void configureMessageBroker(MessageBrokerRegistry config) {
		config.enableSimpleBroker("/msg","/post_topic","/msgid");
		config.setApplicationDestinationPrefixes("/app");
	}
	
	
	@Override
	public void registerStompEndpoints(StompEndpointRegistry stompReg) {
		stompReg.addEndpoint("/ws-free").setAllowedOrigins("*").withSockJS();
	}
}
