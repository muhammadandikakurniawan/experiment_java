package free.project.config;

import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootConfiguration
@EnableTransactionManagement
@EnableJpaRepositories(
		basePackages = "free.project.repository",
		entityManagerFactoryRef = "freeEntityManager", 
		transactionManagerRef = "freeTransactionManager"
		)
public class DataSourceConfig {
	
	@Bean("freeDataSource")
	public DataSource freeDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
	      dataSource.setDriverClassName("org.postgresql.Driver");
	      dataSource.setUrl("jdbc:postgresql://localhost:5432/latihan");
	      dataSource.setUsername("andi");
	      dataSource.setPassword("humorismenalws06");
		
		return dataSource;
	}
	
	@Bean("freeJdbcTemplate")
	public JdbcTemplate freeJdbcTemplate(@Qualifier("freeDataSource") DataSource ds) {
		return new JdbcTemplate(ds);
	}
	
    @Bean(name="freeEntityManager")
    public LocalContainerEntityManagerFactoryBean multi1EntityManager() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(this.freeDataSource());
        em.setPackagesToScan(new String[] { "free.project.model" });
 
        HibernateJpaVendorAdapter vendorAdapter= new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setPersistenceUnitName("freeEntityManager");
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto","update");
        properties.put("hibernate.dialect","org.hibernate.dialect.PostgreSQLDialect");
        em.setJpaPropertyMap(properties);
 
        return em;
    }
    
    
    @Bean(name="freeTransactionManager")
    public PlatformTransactionManager dbTransactionManager(@Qualifier("freeEntityManager") LocalContainerEntityManagerFactoryBean em) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(em.getObject());
        return transactionManager;
    }
    
	
}
