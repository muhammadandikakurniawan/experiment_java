package free.project.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import free.project.batch.job.FreeJobOne;
import free.project.batch.step.*;

@Configuration
@EnableBatchProcessing
public class BatchConfig {
	
	@Autowired
	private JobBuilderFactory jobs;
	
	@Autowired
	private StepBuilderFactory steps;
	
	@Bean
	public Step stepOne() {
		return this.steps.get("step_one")
				.tasklet(new FreeStepOne())
				.build();
	}
	@Bean
	public Step stepTwo() {
		return this.steps.get("step_one")
				.tasklet(new FreeStepTwo())
				.build();
	}
	
	@Bean
	public Job demoJob() {
		return this.jobs.get("jobOne")
				.incrementer(new RunIdIncrementer())
				.listener(new FreeJobOne())
				.start(this.stepOne())
				.next(this.stepTwo())
				.build();
					
	}
	
}
