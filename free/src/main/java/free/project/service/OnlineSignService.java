package free.project.service;

import java.io.File;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;

import com.fasterxml.jackson.databind.ObjectMapper;

import free.project.dao.OtpCodeDao;
import free.project.dao.impl.SignatureRepoImpl;
import free.project.dto.GenerateCodeDTO;
import free.project.dto.SignServiceDTO;
import free.project.model.SignatureModel;
import free.project.repository.AgenRepo;
import free.project.repository.ClientRepo;
import free.project.util.FileUtil;

@Service
public class OnlineSignService {
	
	@Autowired
	private OtpCodeDao otpCodeDao;
	
	@Autowired
	private AgenRepo agenRepo;
	
	@Autowired
	private ClientRepo clientRepo;
	
	@Autowired
	private FileUtil fileUtil;
	
	@Autowired
	private SignatureRepoImpl signatureRepoImpl;
	
	private ObjectMapper objMapper = new ObjectMapper();
	
	
	@Transactional(rollbackFor = {Exception.class},propagation=Propagation.REQUIRED)
	public String saveOtpCode(GenerateCodeDTO codeObj) {
		String res = null;
		
		try {
			
		
			System.out.println(objMapper.writeValueAsString(this.agenRepo.getById(codeObj.getAgenCode())));
			System.out.println(objMapper.writeValueAsString(this.clientRepo.getById(codeObj.getClientCode())));
			
			//roll back if data client and agen is not exist in database
			if(this.agenRepo.getById(codeObj.getAgenCode()) != null && this.clientRepo.getById(codeObj.getClientCode()) != null ) {
				Timestamp expireCode = new Timestamp(System.currentTimeMillis()+((60*60*24)*1000));
				res = this.generateRandomCode(8);
				
//				check code is already or no, if already code will be generated again
				boolean otpCodIsEmpty = this.otpCodeDao.getCodeByCode(res).isEmpty();

				System.out.println(this.objMapper.writeValueAsString(this.otpCodeDao.getCodeByCode(res)));
				
				if(otpCodIsEmpty) {
					
					//save data 
					if(this.otpCodeDao.saveOtpCode(codeObj, res, expireCode) != 1) {
						res = null;
					}	
					
				}else {
					
					while(!otpCodIsEmpty) {
						
						res = this.generateRandomCode(8);
						otpCodIsEmpty = this.otpCodeDao.getCodeByCode(res).isEmpty();
						
						System.out.println(this.objMapper.writeValueAsString(this.otpCodeDao.getCodeByCode(res)));
						
						if(otpCodIsEmpty) {
							//save data 
							if(this.otpCodeDao.saveOtpCode(codeObj, res, expireCode) != 1) {
								res = null;
							}
							
							break;
						}

					}
					
				}
				
			}else {
				System.out.println("========================= client and or agen is not exist =============================");
//				TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
//				TransactionInterceptor.currentTransactionStatus().flush();
			}
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return res;
	}
	
	public String generateRandomCode(Integer length) {
 		Random rand = new Random();
		List<String> listRand = new ArrayList<String>();
        for(Integer i = 0; i < length; i++){
            listRand.add(rand.nextInt(10)+"");
        }
        String co = String.join("", listRand);
        
        return co;
	}
	
	public Boolean validationOtpCode(GenerateCodeDTO params) {
		Boolean res = true;
		Date today = new Date();
		try {
			System.out.println("======================== check code agen client ====================");
			Map<String,Object> dataSign = this.otpCodeDao.checkCodeAgenClient(params);
			System.out.println(this.objMapper.writeValueAsString(dataSign));
			if(dataSign == null) {
				res = false;
			}else {
				System.out.println("expird toke = "+dataSign.get("otp_expire"));
				Long expiredCode = ((Timestamp)dataSign.get("otp_expire")).getTime();
				Long todayTime = today.getTime();
				System.out.println("expird toke = "+expiredCode+"\nToday time = "+todayTime);
				if(expiredCode <= todayTime) {
					res = false;
				}
			}
		}catch(Exception ex) {
			ex.printStackTrace();
			res = false;
		}
		return res;
	}
	
	@Transactional(rollbackFor = {Exception.class},propagation=Propagation.REQUIRED)
	public Boolean saveOlineSign(SignServiceDTO param) {
		Boolean res = false;
		System.out.println("======================== SAVE ONLINE SIGN ====================");
		try {
			
			GenerateCodeDTO paramValidationOtp = new GenerateCodeDTO()
					.setAgenCode(param.getAgenCode())
					.setClientCode(param.getClientCode())
					.setOtpCode(param.getOtpCode());
			
			if(this.validationOtpCode(paramValidationOtp)) {
				Date date = new Date();
				SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy-HH-mm-ss-SSS");
				String signFileName = param.getClientCode().concat(dateFormat.format(date));
				String extFile = this.fileUtil.getExtentionFileBase64(param.getSignature().getStringByte());
				String fileSignName = new File("src\\main\\resources\\files\\").getAbsoluteFile()+"\\"+signFileName+"."+extFile;
				
				
				System.out.println("======================== check file sign exist ====================");
				System.out.println(fileSignName);
				System.out.println(this.fileUtil.isExistSign(fileSignName));
				
				if(this.fileUtil.convertBase64ToFile(param.getSignature().getStringByte(),fileSignName)) {
					Boolean saveSign = this.signatureRepoImpl.saveSign(new SignatureModel()
							.setAgen_id(param.getAgenCode())
							.setClient_id(param.getClientCode())
							.setOtp_code(param.getOtpCode())
							.setSign_file(signFileName)
							.setSign_location(param.getSignature().getLocation())
							.setSign_time(param.getSignature().getDate()));
					res = saveSign;
					
				}
			}
			
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return res;
	}
	
}
