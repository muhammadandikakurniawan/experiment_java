package free.project.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import free.project.dao.impl.AgenRepoImpl;
import free.project.dao.impl.ClientRepoImpl;
import free.project.model.AgenModel;
import free.project.model.ClientModel;

@Service
public class CmsService {

	@Autowired
	private AgenRepoImpl agenImpl;

	@Autowired
	private ClientRepoImpl clientImpl;
	
	public Integer addAgen(AgenModel param) {
		Integer res = 0;
		String AgenId = String.format("AG-%s", Long.toString(new Date().getTime()));
		param.setAgen_id(AgenId);
		try {
			
			res = this.agenImpl.save(param);;
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return res;
	}
	
	public Integer addClient(ClientModel param) {
		Integer res = 0;
		String client_id = String.format("CL-%s", Long.toString(new Date().getTime()));
		param.setClient_id(client_id);
		try {
			
			res = this.clientImpl.save(param);;
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return res;
	}
	
}
