package free.project.service.async;

import java.util.concurrent.CompletableFuture;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@EnableAsync
@Service
public class Service1Async {
	
//	@Scheduled(fixedDelay = 1000)
    @Async
	public CompletableFuture<Integer> Print1(){
		Integer res = 1;
		
		for(Integer i = 0; i < 10; i++) {
			System.out.println("========================= PRINT1 ====================");
		}
		return CompletableFuture.completedFuture(res);
	}
	
//	@Scheduled(fixedDelay = 1000)
    @Async
	public CompletableFuture<Integer> Print2(){
		Integer res = 2;
		for(Integer i = 0; i < 15; i++) {
			System.out.println("========================= PRINT2 ====================");
		}
		return CompletableFuture.completedFuture(res);
	}
	
}
