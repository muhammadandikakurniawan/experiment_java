package free.project.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import free.project.model.AgenModel;

@Repository
public interface AgenRepo extends JpaRepository<AgenModel,String>{
	
	@Query(value="SELECT * FROM onlinesign.tb_agen ag WHERE ag.agen_id = :id",nativeQuery=true)
	public AgenModel getById(String id);
	
	@Query(value="SELECT * FROM onlinesign.tb_agen ag WHERE ag.agen_email = :email",nativeQuery=true)
	public AgenModel getByEmail(String email);
	
}
