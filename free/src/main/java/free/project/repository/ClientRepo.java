package free.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import free.project.model.ClientModel;
import free.project.model.SignatureModel;

@Repository
public interface ClientRepo extends JpaRepository<ClientModel,String>{
	
	@Query(value="SELECT * FROM onlinesign.tb_client WHERE client_id = :id",nativeQuery=true)
	public ClientModel getById(String id);
	
}
