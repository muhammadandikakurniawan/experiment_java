package free.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import free.project.model.SignatureModel;

@Repository
public interface SignatureRepo extends JpaRepository<SignatureModel,Integer>{
	
//	@Query(value="INSERT INTO onlinesign.tb_signature(gen_id, client_id, otp_code, sign_file, sign_location, sign_time) VALUES(:gen_id, :client_id, :otp_code, :sign_file, :sign_location, :sign_time)",nativeQuery=true)
//	public Object insertInto(String code);
	
}
