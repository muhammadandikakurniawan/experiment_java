package free.project.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;

import free.project.dto.GenerateCodeDTO;

@Service
public class OtpCodeDao {
	
	@Autowired
	@Qualifier("freeJdbcTemplate")
	private JdbcTemplate jdbcTmpl;
	
	private ObjectMapper objMapper = new ObjectMapper();
	
	@Transactional(rollbackFor = {Exception.class},propagation=Propagation.REQUIRED)
	public Integer saveOtpCode(GenerateCodeDTO param,String otpCode,Timestamp expire) {
		Integer res = 0;

		try {
			String query = String.format("INSERT INTO onlinesign.tb_otp_online_sign (agen_id, client_id, otp_code, otp_expire) VALUES(%s)", String.join(",", Collections.nCopies(4, "?")));
			res = this.jdbcTmpl.update(query,new PreparedStatementSetter() {

				@Override
				public void setValues(PreparedStatement ps) throws SQLException {
					ps.setString(1, param.getAgenCode());
					ps.setString(2, param.getClientCode());
					ps.setString(3, otpCode);
					ps.setTimestamp(4, expire);
				}
				
			});
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return res;
	}
	
	@Transactional(rollbackFor = {Exception.class},propagation=Propagation.REQUIRED)
	public ArrayList<Map<String,Object>> getCodeByCode(String param) throws Exception{
		ArrayList<Map<String,Object>> res = null;
		try {
			String query = String.format("SELECT * FROM onlinesign.tb_otp_online_sign WHERE otp_code = %s", "?");
			System.out.println("======================================== Result set ========================================");
			res = (ArrayList<Map<String, Object>>) this.jdbcTmpl.queryForList(query, param);
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return res;
	}
	
	public Map<String,Object> checkCodeAgenClient(GenerateCodeDTO param){
		Map<String,Object> res = null;
		try {
			String query = String.format("SELECT * FROM onlinesign.tb_otp_online_sign WHERE otp_code = %s AND client_id = %s AND agen_id = %s ", '?','?','?');
			res = this.jdbcTmpl.queryForMap(query, new Object[] {param.getOtpCode(), param.getClientCode(), param.getAgenCode()});
			
			
		}catch(EmptyResultDataAccessException ex) {
			res = null;
		}
		return res;
	}
	
}
