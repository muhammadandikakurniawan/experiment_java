package free.project.dao.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import free.project.model.AgenModel;
import free.project.model.ClientModel;
import free.project.repository.ClientRepo;

@Service
public class ClientRepoImpl {

	@Autowired
	private ClientRepo clientRepo;
	
	public Integer save(ClientModel param) {
		Integer res = 0;
		try {
			
			this.clientRepo.save(param);
			
			res = 1;
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return res;
	}
	
}
