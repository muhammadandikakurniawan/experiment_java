package free.project.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import free.project.model.SignatureModel;
import free.project.repository.SignatureRepo;

@Service
public class SignatureRepoImpl {
	
	@Autowired
	private SignatureRepo signatureRepo;
	
	@Transactional(rollbackFor = {Exception.class},propagation=Propagation.REQUIRED)
	public Boolean saveSign(SignatureModel param) {
		Boolean res = false;
		try {
			SignatureModel s =  this.signatureRepo.save(param);
			res = true;
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return res;
	}
	
}
