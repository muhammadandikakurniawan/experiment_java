package free.project.dao.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import free.project.model.AgenModel;
import free.project.repository.AgenRepo;

@Service
public class AgenRepoImpl{

	@Autowired
	AgenRepo agenRepo;
	
	public Integer save(AgenModel param) {
		Integer res = 0;
		try {
			
			this.agenRepo.save(param);
			
			res = 1;
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return res;
	}
	


}
