package free.project;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import free.project.service.async.Service1Async;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableScheduling
@EnableAsync
public class FreeApplication extends SpringBootServletInitializer{
	
	@Autowired
	Service1Async serAsync1;
	
	@Autowired
	JobLauncher jobLauncer;
	
	@Autowired
	Job job;
	
	public static void main(String[] args) {
//		Service1Async serAsync1 = new Service1Async();
//		serAsync1.Print1();
//		System.out.println("========================= =========== ====================");
//		serAsync1.Print2();
		
		SpringApplication.run(FreeApplication.class, args);
	}
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(FreeApplication.class);
	}
//	@Scheduled(cron="0/10 * * * *  ?")
//	public void performJob() throws JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException, JobParametersInvalidException {
//		JobParameters jobParams = new JobParametersBuilder()
//				.addString("jobOne", String.valueOf(System.currentTimeMillis()))
//				.toJobParameters();
//		jobLauncer.run(job, jobParams);
//	}

}
