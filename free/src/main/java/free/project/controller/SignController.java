package free.project.controller;

import java.util.HashMap;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import free.project.dto.GenerateCodeDTO;
import free.project.dto.SignServiceDTO;
import free.project.model.AgenModel;
import free.project.model.ClientModel;
import free.project.service.CmsService;
import free.project.service.OnlineSignService;
import free.project.util.FileUtil;

@CrossOrigin("*")
@RestController
@RequestMapping(value="/signService")
public class SignController {
	
	@Autowired
	private CmsService cmsService;
	
	@Autowired
	private OnlineSignService onlineSignService;

	
	@RequestMapping(value="/checkValidCode",method=RequestMethod.POST)
	public HashMap<String,Object> checkValidCode(@RequestBody() GenerateCodeDTO param){
		HashMap<String,Object> res = new HashMap<String,Object>();
		ObjectMapper objMapper = new ObjectMapper();
		try {
			
			System.out.println(objMapper.writeValueAsString(param));
			res.put("response_code", Response.SC_OK);
			res.put("message", "valid");
			res.put("isValid", this.onlineSignService.validationOtpCode(param));
		}catch(Exception ex) {
			res.put("response_code", Response.SC_NON_AUTHORITATIVE_INFORMATION);
			res.put("error_message", ex.getMessage());
			ex.printStackTrace();
		}
		
		return res;
	}
	
	@RequestMapping(value="/sendOnlineSign",method=RequestMethod.POST)
	public HashMap<String,Object> sendOnlineSign(@RequestBody() SignServiceDTO param) throws JsonProcessingException{
		HashMap<String,Object> res = new HashMap<String,Object>();
		ObjectMapper objMapper = new ObjectMapper();

		try {
			this.onlineSignService.saveOlineSign(param);
			res.put("response_code", Response.SC_OK);
			res.put("message", "valid");
			res.put("param", objMapper.writeValueAsString(param));
		}catch(Exception ex) {
			res.put("response_code", Response.SC_NON_AUTHORITATIVE_INFORMATION);
			res.put("error_message", ex.getMessage());
			ex.printStackTrace();
		}
		
		System.out.println(objMapper.writeValueAsString(param));
		
		return res;
	}	
	
	@RequestMapping(value="/addAgen",method=RequestMethod.POST)
	public HashMap<String,Object> addAgen(@RequestBody() AgenModel param){
		HashMap<String,Object> res = new HashMap<String,Object>();
		ObjectMapper objMapper = new ObjectMapper();
		try {
			System.out.println(objMapper.writeValueAsString(param));
			if(this.cmsService.addAgen(param) == 1) {
				res.put("response_code", Response.SC_OK);
				res.put("message", "success");
			}else {
				res.put("response_code", Response.SC_INTERNAL_SERVER_ERROR);
				res.put("message", "failed");
			}
		}catch(Exception ex) {
			res.put("response_code", Response.SC_NON_AUTHORITATIVE_INFORMATION);
			res.put("error_message", ex.getMessage());
			ex.printStackTrace();
		}
		
		return res;
	}
	
	@RequestMapping(value="/addClient",method=RequestMethod.POST)
	public HashMap<String,Object> addClient(@RequestBody() ClientModel param){
		HashMap<String,Object> res = new HashMap<String,Object>();
		ObjectMapper objMapper = new ObjectMapper();
		try {
			System.out.println(objMapper.writeValueAsString(param));
			if(this.cmsService.addClient(param) == 1) {
				res.put("response_code", Response.SC_OK);
				res.put("message", "success");
			}else {
				res.put("response_code", Response.SC_INTERNAL_SERVER_ERROR);
				res.put("message", "failed");
			}
		}catch(Exception ex) {
			res.put("response_code", Response.SC_NON_AUTHORITATIVE_INFORMATION);
			res.put("error_message", ex.getMessage());
			ex.printStackTrace();
		}
		
		return res;
	}
	
	@RequestMapping(value="/generateCode",method=RequestMethod.POST)
	public HashMap<String,Object> generateCode(@RequestBody() GenerateCodeDTO param){
		HashMap<String,Object> res = new HashMap<String,Object>();
		ObjectMapper objMapper = new ObjectMapper();
		try {
			System.out.println(objMapper.writeValueAsString(param));
			String saveOtp = this.onlineSignService.saveOtpCode(param);
			if(saveOtp != null) {
				res.put("response_code", Response.SC_OK);
				res.put("message", "success");
				res.put("otp_code", saveOtp);
			}else {
				res.put("response_code", Response.SC_INTERNAL_SERVER_ERROR);
				res.put("message", "failed");
			}
		}catch(Exception ex) {
			res.put("response_code", Response.SC_NON_AUTHORITATIVE_INFORMATION);
			res.put("error_message", ex.getMessage());
			ex.printStackTrace();
		}
		
		return res;
	}
	
}
