package free.project.controller;

import java.util.HashMap;
import java.util.Properties;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import free.project.dto.EmailDto;

@CrossOrigin("*")
@RestController
@RequestMapping(value="/testCtrl")
public class TestController {
	
	@Autowired
	@Qualifier("freemailSender")
	public JavaMailSender emailSender;
	
	@RequestMapping(value="/sendEmail",method=RequestMethod.POST)
	public HashMap<String,Object> sendEmail(@RequestBody EmailDto param){
		HashMap<String,Object> res = new HashMap<String,Object>();
		ObjectMapper objMapper = new ObjectMapper();
		try {
			System.out.println(objMapper.writeValueAsString(param));
			
			//prepare set email from
			JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
			
			mailSender.setHost("smtp.gmail.com");
			mailSender.setPort(587);
			mailSender.setPassword(param.getPassword());
			mailSender.setUsername(param.getFrom());
		    Properties props = mailSender.getJavaMailProperties();
		    props.put("mail.transport.protocol", "smtp");
		    props.put("mail.smtp.auth", "true");
		    props.put("mail.smtp.starttls.enable", "true");
		    props.put("mail.debug", "true");
			
			//prepare messsage
			SimpleMailMessage mailMsg = new SimpleMailMessage();
			mailMsg.setTo(param.getTo());
			mailMsg.setSubject("from = "+param.getFrom());
			mailMsg.setText(param.getMsg());
			//send email
			mailSender.send(mailMsg);
			
			res.put("response_code", Response.SC_OK);
			res.put("message", "valid");
			res.put("isValid", "true");
		}catch(Exception ex) {
			res.put("response_code", Response.SC_NON_AUTHORITATIVE_INFORMATION);
			res.put("error_message", ex.getMessage());
			ex.printStackTrace();
		}
		
		return res;
	}
	
	@RequestMapping(value="/test",method=RequestMethod.GET)
	public HashMap<String,Object> tst(){
		HashMap<String,Object> res = new HashMap<String,Object>();
		
		res.put("message", "hello world");
		
		return res;
	}
	
}
