package free.project.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@CrossOrigin("*")
public class Ws1Controller {
	
    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;
    
    @RequestMapping(value="/sendmsg",method=RequestMethod.POST)
    @CrossOrigin("*")
    public Integer sendMsg(@RequestBody HashMap<String,Object> params) throws JsonProcessingException {
    	Integer result = 0;
    	System.out.println((new ObjectMapper()).writeValueAsString(params));
    	try {
        	ObjectMapper objMapper = new ObjectMapper();
        	String dst = params.get("dst").toString();
        	String msg = params.get("msg").toString();
        	this.simpMessagingTemplate.convertAndSend("/msg",msg);
        	result = 1;
    	}catch(Exception ex) {
    		ex.printStackTrace();
    	}
    	return result;
    }
    @RequestMapping(value="/sendid",method=RequestMethod.POST)
    @CrossOrigin("*")
    public Integer sendId(@RequestBody HashMap<String,Object> params) throws JsonProcessingException {
    	Integer result = 0;
    	System.out.println((new ObjectMapper()).writeValueAsString(params));
    	try {
        	ObjectMapper objMapper = new ObjectMapper();
        	String msg = objMapper.writeValueAsString(params);
        	this.simpMessagingTemplate.convertAndSend("/msgid",msg);
        	result = 1;
    	}catch(Exception ex) {
    		ex.printStackTrace();
    	}
    	return result;
    }
	
    @MessageMapping("/msg")
	public void getPost(String mess) {
    	ObjectMapper objMapper = new ObjectMapper();
    	System.out.println("=========================================== ws ===========================================");
    	System.out.println(mess);
    	System.out.println("=========================================== ws ===========================================");
    	
    	this.simpMessagingTemplate.convertAndSend("/msg",mess);
    }
    
    @MessageMapping("/msgid")
	public void msgid(String mess) {
    	ObjectMapper objMapper = new ObjectMapper();
    	System.out.println("=========================================== ws ===========================================");
    	System.out.println(mess);
    	System.out.println("=========================================== ws ===========================================");
    	
    	this.simpMessagingTemplate.convertAndSend("/msgid",mess);
    }
    
    
}
