package free.project.model;

import java.sql.Timestamp;

import javax.persistence.*;

@Entity
@Table(name="tb_signature",schema="onlinesign")
public class SignatureModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer Id;
	private String agen_id;
	private String client_id;
	private String otp_code;
	private String sign_file;
	private String sign_location;
	private Timestamp sign_time;
	
	public Integer getId() {
		return Id;
	}
	public SignatureModel setId(Integer id) {
		Id = id;
		return this;
	}
	public String getAgen_id() {
		return agen_id;
	}
	public SignatureModel setAgen_id(String agen_id) {
		this.agen_id = agen_id;
		return this;
	}
	public String getClient_id() {
		return client_id;
	}
	public SignatureModel setClient_id(String client_id) {
		this.client_id = client_id;
		return this;
	}
	public String getOtp_code() {
		return otp_code;
	}
	public SignatureModel setOtp_code(String otp_code) {
		this.otp_code = otp_code;
		return this;
	}
	public String getSign_file() {
		return sign_file;
	}
	public SignatureModel setSign_file(String sign_file) {
		this.sign_file = sign_file;
		return this;
	}
	public String getSign_location() {
		return sign_location;
	}
	public SignatureModel setSign_location(String sign_location) {
		this.sign_location = sign_location;
		return this;
	}
	public Timestamp getSign_time() {
		return sign_time;
	}
	public SignatureModel setSign_time(Timestamp sign_time) {
		this.sign_time = sign_time;
		return this;
	}
	
}
