package free.project.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_agen",schema="onlinesign")
public class AgenModel {
	
	@Id
	private String agen_id;
	private String agen_email;
	private String agen_password;
	private String agen_name;
	
	public String getAgen_id() {
		return agen_id;
	}
	public void setAgen_id(String agen_id) {
		this.agen_id = agen_id;
	}
	public String getAgen_email() {
		return agen_email;
	}
	public void setAgen_email(String agen_email) {
		this.agen_email = agen_email;
	}
	public String getAgen_password() {
		return agen_password;
	}
	public void setAgen_password(String agen_password) {
		this.agen_password = agen_password;
	}
	public String getAgen_name() {
		return agen_name;
	}
	public void setAgen_name(String agen_name) {
		this.agen_name = agen_name;
	}
	
}
