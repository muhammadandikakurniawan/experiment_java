package com.multidb2;

import java.util.HashMap;
import java.util.List;

import javax.sql.DataSource;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@SpringBootTest
class Multidb2ApplicationTests {

	@Autowired
	private ObjectMapper _objMapper;

	@Test
	void contextLoads() {
	}


	@Test
	void JdbcTemplate_test() {
		DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setDriverClassName("org.postgresql.Driver");
		ds.setUrl("jdbc:postgresql://localhost:5432/db_training");
		ds.setUsername("postgres");
		ds.setPassword("admin");
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(ds);

		List<HashMap<String,Object>> datas = jdbcTemplate.query("SELECT * FROM training.tb_product",
				new Object[] {}, (data, rowNum) -> {
			return new HashMap<String,Object>(){{
				put("product_name", data.getString("name"));
				put("product_price", data.getString("price"));
				put("product_code", data.getString("sku_code"));
			}};
		});

		String dbg = "";
	}

}
