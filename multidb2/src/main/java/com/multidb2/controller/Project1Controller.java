package com.multidb2.controller;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

import javax.servlet.http.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.transaction.annotation.*;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.multidb2.dao.UserHashRepoImpl;
import com.multidb2.dto.UserDto;
import com.multidb2.entity.mongo1.UserDocument;
import com.multidb2.entity.multi1.User1Entity;
import com.multidb2.entity.multi2.User2Entity;
import com.multidb2.hash.UserHash;
import com.multidb2.repository.mongo1.UserRepo;
import com.multidb2.repository.multi1.User1Repo;
import com.multidb2.repository.multi2.User2Repo;
import com.multidb2.util.JwtUtil;

@RestController
@RequestMapping("/project")
@CrossOrigin()
public class Project1Controller {
	
	@Qualifier("multi1JdbcTemplate")
	@Autowired
	private JdbcTemplate multi1JdbcTmp;
	
	@Qualifier("multi2JdbcTemplate")
	@Autowired
	private JdbcTemplate multi2JdbcTmp;
	
	@Autowired
	private User1Repo user1repo;
	
	@Autowired
	private User2Repo user2repo;
	
	@Autowired
	private UserRepo mongoUser1Repo;
	
	@Autowired
	@Qualifier("mongoTemplate")
	private MongoTemplate mongoTmpl;
	
	@Autowired
	private UserHashRepoImpl userDao;
	
	@Autowired
	private JwtUtil jwtUtil;
	
	@RequestMapping(value="/insert1",method=RequestMethod.POST)
	@Transactional(rollbackFor = {Exception.class},propagation=Propagation.REQUIRED)
	public HashMap<String,Object> insert1(@RequestBody() HashMap<String,Object> param,HttpServletResponse httpresponse,HttpServletRequest httprequest) throws JsonProcessingException{
		HashMap<String,Object> result = new HashMap<>();
		ObjectMapper objMap = new ObjectMapper();
		try {
			this.user1repo.save((new User1Entity(objMap.writeValueAsString(param))));
			param.put("user_id",new String().format("USR-%s", System.nanoTime()));
			String qMarks = new String().join(",", Collections.nCopies(1, "?"));
			String query = new String().format("insert into public.tb_user(data) values (%s)", qMarks);
//			this.multi1JdbcTmp.update(query,new PreparedStatementSetter() {
////				
//				public void setValues(PreparedStatement ps) throws SQLException {
////					for(Integer i = 1; i <= 1; i++) {
//							try {
//								ps.setString(1, objMap.writeValueAsString(param));
////								ps.setInt(2, Integer.parseInt(param.get("id").toString()));
//							} catch (JsonProcessingException e) {
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//							}
////					}
//				}
//				
//			});
			
			result.put("status","success");
			result.put("code",httpresponse.hashCode());
			System.out.println("======================== data ====================");
			System.out.println(objMap.writeValueAsString(param));
			System.out.println("======================== result =======================");
			System.out.println(objMap.writeValueAsString(result));
		}catch(Exception ex) {
			result.put("status","error");
			result.put("error_message",ex.getMessage());
			result.put("code",httpresponse.hashCode());
		}
		
		return result;
	}
	
	@RequestMapping(value="/insert2",method=RequestMethod.POST)
	@Transactional(rollbackFor = {Exception.class},propagation=Propagation.REQUIRED)
	public HashMap<String,Object> insert2(@RequestBody() HashMap<String,Object> param,HttpServletResponse httpresponse,HttpServletRequest httprequest) throws JsonProcessingException{
		HashMap<String,Object> result = new HashMap<>();
		ObjectMapper objMap = new ObjectMapper();
		try {
			this.user2repo.save((new User2Entity(objMap.writeValueAsString(param))));
			param.put("user_id",new String().format("USR-%s", System.nanoTime()));
			String qMarks = new String().join(",", Collections.nCopies(1, "?"));
			String query = new String().format("insert into public.tb_user(user_data) values (%s)", qMarks);
//			this.multi2JdbcTmp.update(query,new PreparedStatementSetter() {
////				
//				public void setValues(PreparedStatement ps) throws SQLException {
////					for(Integer i = 1; i <= 1; i++) {
//							try {
//								ps.setString(1, objMap.writeValueAsString(param));
////								ps.setInt(2, Integer.parseInt(param.get("id").toString()));
//							} catch (JsonProcessingException e) {
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//							}
////					}
//				}
//				
//			});
			
			result.put("status","success");
			result.put("code",httpresponse.hashCode());
			System.out.println("======================== data ====================");
			System.out.println(objMap.writeValueAsString(param));
			System.out.println("======================== result =======================");
			System.out.println(objMap.writeValueAsString(result));
		}catch(Exception ex) {
			result.put("status","error");
			result.put("error_message",ex.getMessage());
			result.put("code",httpresponse.hashCode());
		}
		
		return result;
	}

	@RequestMapping(value="/redis_save",method=RequestMethod.POST)
	public HashMap<String,Object> redis_save(@RequestBody() UserHash param){
		HashMap<String,Object> result = new HashMap<>();
		ObjectMapper objMap = new ObjectMapper();
		try {
			
			Integer data = this.userDao.save(param);
			
			result.put("status","success");
			result.put("process",data);
			System.out.println("======================== data ====================");
			System.out.println(objMap.writeValueAsString(param));
			System.out.println("======================== result =======================");
			System.out.println(objMap.writeValueAsString(result));
		}catch(Exception ex) {
			result.put("status","error");
			result.put("error_message",ex.getMessage());
		}
		return result;
	}

	@RequestMapping(value="/mongo_save",method=RequestMethod.POST)
	public HashMap<String,Object> mongoSave(@RequestBody() UserDocument params){
		HashMap<String,Object> result = new HashMap<String,Object>();
		ObjectMapper objMapper = new ObjectMapper();
		try {
//			UserDocument process = this.mongoTmpl.save(params);
			UserDocument process = this.mongoUser1Repo.save(params);
			if(process == null) {
				result.put("process", 0);
				result.put("status", "failed");
			}else {
				result.put("process", 100);
				result.put("status", "success");
			}
			
		}catch(Exception ex) {
			result.put("process", -1);
			result.put("error_massage", ex.getMessage());
			result.put("status", "error");
		}
		
		return result;
	}
	
	@RequestMapping(value="/jsonProp",method=RequestMethod.POST)
	public HashMap<String,Object> jsonProp(@RequestBody() UserDto param){
		ObjectMapper objMapper = new ObjectMapper();
		HashMap<String,Object> result = new HashMap<String,Object>();
		
		result = objMapper.convertValue(param, HashMap.class);
		
		return result;
	}
	
	@RequestMapping(value="/savetoken",method=RequestMethod.POST)
	public HashMap<String,Object> saveToken(@RequestBody() UserDto param){
		ObjectMapper objMap = new ObjectMapper();
		HashMap<String,Object> result = new HashMap<String,Object>();
		try {
			
			Date expireToken = new Date(new Date().getTime() + 30000); //30 second
			
			param.setUserId(new String().format("USER-%s", System.nanoTime()));
			HashMap<String,Object> payloadToken = new HashMap<String,Object>();
			payloadToken = objMap.convertValue(param, HashMap.class);
			payloadToken.put("subject", param.getUserName());
			
			payloadToken.put("token",this.jwtUtil.Builder(payloadToken, param.getUserPassword()).setExpiration(expireToken).compact());
			
			Integer data = this.userDao.saveToken(payloadToken);
			
			
			result.put("process",data);
			
			if(data == 0) {
				result.put("status","failed");
			}
			else if(data < 0) {
				result.put("status","error");
			}
			
			System.out.println("============================= PARAM HASH =================================");
			
			System.out.println("============================= PARAM HASH =================================");
			UserDto paramdto = objMap.convertValue(payloadToken, UserDto.class);
			paramdto.setUserId(new String().format("USER-%s", System.nanoTime()));
			System.out.println(paramdto.getUserId());
			System.out.println(paramdto.getUserName());
			System.out.println(paramdto.getUserEmail());
			System.out.println(paramdto.getUserPassword());
			
			System.out.println("======================== data ====================");
			System.out.println(objMap.writeValueAsString(param));
			System.out.println("======================== payloadToken ====================");
			System.out.println(objMap.writeValueAsString(payloadToken));
			System.out.println("======================== result =======================");
			System.out.println(objMap.writeValueAsString(result));
		}catch(Exception ex) {
			result.put("status","error");
			result.put("error_message",ex.getMessage());
		}
		return result;
	}
	
	@RequestMapping(value="/gettoken",method=RequestMethod.POST)
	public HashMap<String,Object> getToken(@RequestBody() UserDto param){
		ObjectMapper objMap = new ObjectMapper();
		HashMap<String,Object> result = new HashMap<String,Object>();
		try {
			
			String resultData = this.userDao.getStringUserTokenByKey(param.getUserName());
			
			if(!resultData.equals("")) {
				
				HashMap<String,Object> dataTokenUser = objMap.readValue(resultData, HashMap.class);
				String dataToken = dataTokenUser.get("token").toString();
				
				System.out.println("======================== CLAIMS ====================");
				System.out.println(objMap.writeValueAsString(this.jwtUtil.getClaims(dataToken, dataTokenUser.get("user_password").toString())));
				
				if(dataTokenUser.get("user_password").toString().equals(param.getUserPassword())) {
					result.put("status","success");
					result.put("data",resultData);
					result.put("claims", objMap.writeValueAsString(this.jwtUtil.getClaims(dataToken, dataTokenUser.get("user_password").toString())));
				}else {
					result.put("status","password is not valid");
				}
				
			}else {
				result.put("status","failed");
			}
			
			System.out.println("======================== data ====================");
			System.out.println(objMap.writeValueAsString(param));
			System.out.println("======================== result =======================");
			System.out.println(objMap.writeValueAsString(result));
		}catch(Exception ex) {
			result.put("status","error");
			result.put("error_message",ex.getMessage());
		}
		return result;
	}
}
