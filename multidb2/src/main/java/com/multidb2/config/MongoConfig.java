package com.multidb2.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.MongoClient;

@Configuration
@EnableMongoRepositories("com.multidb2.repository.mongo1")
public class MongoConfig{

//	@Override
//	public MongoClient mongoClient() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	protected String getDatabaseName() {
//		// TODO Auto-generated method stub
//		return "latihan";
//	}
//	
	@Bean("mongoTemplate")
	public MongoTemplate mongoTmp() {
		return new MongoTemplate(mongoClient1(),"latihan");
	}
	

    @Bean("mongoClient1")
    public MongoClient mongoClient1() {
    	
        MongoClient client = new MongoClient("localhost", 27017);

        return client;
    }

}
