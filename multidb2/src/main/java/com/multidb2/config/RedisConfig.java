package com.multidb2.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
//@EnableRedisRepositories
public class RedisConfig {
	
//	@Bean(name="jedis1Connection")
//	JedisConnectionFactory jedisConnectionFactory() {
//		RedisStandaloneConfiguration config = new RedisStandaloneConfiguration();
//		config.setHostName("localhost");
//		config.setPort(6379);
//	    return new JedisConnectionFactory(config);
//	}
//	
  @Bean(name="jedis1Connection")
  RedisConnectionFactory connectionFactory() {
		RedisStandaloneConfiguration config = new RedisStandaloneConfiguration();
		config.setHostName("localhost");
		config.setPort(6379);
    return new LettuceConnectionFactory(config);
  }
	 
	@Bean(name="redis1Template")
	public RedisTemplate<?, ?> redisTemplate(@Qualifier("jedis1Connection") RedisConnectionFactory redisConnection){

	    RedisTemplate<String, Object> template = new RedisTemplate<>();
	    RedisSerializer<String> stringSerializer = new StringRedisSerializer();
	    JdkSerializationRedisSerializer jdkSerializationRedisSerializer = new JdkSerializationRedisSerializer();
	    template.setConnectionFactory(redisConnection);
	    template.setEnableTransactionSupport(true);
	    
	    template.setKeySerializer(new GenericToStringSerializer< Object >( Object.class ));
	    template.setHashKeySerializer(new GenericToStringSerializer< Object >( Object.class ));
	    template.setHashValueSerializer(new GenericToStringSerializer< Object >( Object.class));
	    template.setValueSerializer(new GenericToStringSerializer< Object >( Object.class));
	    
	    template.afterPropertiesSet();
	    
	    return template;
	    
	}
	
}
