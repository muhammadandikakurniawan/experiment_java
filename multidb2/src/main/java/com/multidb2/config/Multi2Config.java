package com.multidb2.config;

import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
		basePackages = "com.multidb2.repository.multi2",
		entityManagerFactoryRef = "multi2EntityManager", 
		transactionManagerRef = "multi2TransactionManager"
		)
public class Multi2Config {
	
	@Bean(name="multi2DataSource")
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
      dataSource.setDriverClassName("org.postgresql.Driver");
      dataSource.setUrl("jdbc:postgresql://localhost:5432/multi2");
      dataSource.setUsername("andi");
      dataSource.setPassword("humorismenalws06");

      return dataSource;
	}
	
    @Bean(name="multi2EntityManager")
    public LocalContainerEntityManagerFactoryBean multi2EntityManager() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(this.dataSource());
        em.setPackagesToScan(new String[] { "com.multidb2.entity.multi2" });
 
        HibernateJpaVendorAdapter vendorAdapter= new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto","update");
        properties.put("hibernate.dialect","org.hibernate.dialect.PostgreSQLDialect");
        em.setJpaPropertyMap(properties);
 
//        return builder.dataSource(dataSource).properties(properties).packages("").build()
        return em;
    }

    @Bean(name="multi2TransactionManager")
    public PlatformTransactionManager dbTransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(multi2EntityManager().getObject());
        return transactionManager;
    }
    
    @Bean(name="multi2JdbcTemplate")
    @Autowired
    public JdbcTemplate jdbcTemplate(@Qualifier("multi2DataSource") DataSource datasouce) {
    	return new JdbcTemplate(datasouce);
    }
}
