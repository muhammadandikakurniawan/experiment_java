package com.multidb2.hash;

import java.io.Serializable;

import com.multidb2.dto.UserDto;

public class UserHash implements Serializable{
	
	private String user_id;
	private String user_name;
	private String user_email;
	private String user_password;
	private String data;
	
	public String getUser_password() {
		return user_password;
	}

	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}
	
	
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
	
	public UserHash() {
		
	}
	
	public UserHash(String name, String email) {
		this.user_email = email;
		this.user_name = name;
	}
	
	public UserHash(UserDto param) {
		this.user_email = param.getUserEmail();
		this.user_name = param.getUserName();
		this.user_password = param.getUserPassword();
	}
	
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getUser_email() {
		return user_email;
	}
	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}
	
	
}
