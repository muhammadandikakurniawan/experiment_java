package com.multidb2.repository.redis;

import org.springframework.stereotype.Repository;

import com.multidb2.hash.UserHash;

@Repository
public interface UserHashRepo {
	
	public Integer save(UserHash param);
	
}
