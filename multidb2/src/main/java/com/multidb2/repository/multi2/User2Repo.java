package com.multidb2.repository.multi2;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.multidb2.entity.multi2.User2Entity;

@Repository
//@Qualifier("multi2EntityManager")
public interface User2Repo extends JpaRepository<User2Entity,Integer>{

}
