package com.multidb2.repository.mongo1;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.multidb2.entity.mongo1.UserDocument;

@Repository()
public interface UserRepo extends MongoRepository<UserDocument,String>{

}
