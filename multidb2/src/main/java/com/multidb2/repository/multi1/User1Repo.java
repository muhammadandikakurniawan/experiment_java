package com.multidb2.repository.multi1;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.multidb2.entity.multi1.User1Entity;

@Repository
//@Qualifier("multi1EntityManager")
public interface User1Repo extends JpaRepository<User1Entity,Integer>{

}
