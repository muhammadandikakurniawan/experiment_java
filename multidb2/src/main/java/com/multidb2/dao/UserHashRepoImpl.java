package com.multidb2.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.multidb2.dto.UserDto;
import com.multidb2.hash.UserHash;
import com.multidb2.repository.redis.UserHashRepo;

@Service
public class UserHashRepoImpl implements UserHashRepo{
	
//	@Autowired
//	@Qualifier("redis1Template")
	private RedisTemplate<String,UserHash> redisTemplate;
	
//	@Autowired
	private HashOperations hashOp;
	
	private ListOperations listOp;
	
	public UserHashRepoImpl(RedisTemplate<String,UserHash> redistmpl) {
		this.redisTemplate = redistmpl;
		this.hashOp = this.redisTemplate.opsForHash();
		this.listOp = this.redisTemplate.opsForList();
	}

	public Integer saveToken(HashMap<String,Object> param) throws JsonProcessingException {
		Integer res = 0;
		ObjectMapper objMapper = new ObjectMapper();
		System.out.println("=============== param dao =============");
		System.out.println(objMapper.writeValueAsString(param));
		try {
			UserDto paramdto = objMapper.convertValue(param, UserDto.class);
			param.put("user_id", paramdto.getUserId());
			
			System.out.println("============================= PARAM HASH =================================");
			System.out.println(paramdto.getUserId());
			System.out.println(paramdto.getUserName());
			System.out.println(paramdto.getUserEmail());
			System.out.println(param.get("token"));
			
			List<HashMap<String,Object>> dataUserToken = new ArrayList<>();
			
			dataUserToken.add(param);
			
//			this.listOp.leftPush("token_users",param);

//			this.hashOp.put("token_users", "user_id", paramdto.getUserId());
//			this.hashOp.put("token_users", "user_name", paramdto.getUserName());
//			this.hashOp.put("token_users", "user_password", paramdto.getUserPassword());
//			this.hashOp.put("token_users", "token", param.get("token"));
			
			this.hashOp.put("token_users", paramdto.getUserName(), objMapper.writeValueAsString(param));
			
//			this.hashOp.put("token_users", paramdto.getUserName(), param);
			
//			this.listOp.set("list_user", 1, objMapper.writeValueAsString(param));
//			this.listOp.set("list_user", 1, param);
			res = 100;
//			this.redisTemplate.opsForList(token_users,)
			
		}catch(Exception ex) {
			res = -1;
			ex.printStackTrace();
		}
		
		return res;
	}
	
	@Override
	public Integer save(UserHash param) {
		Integer res = 0;
		ObjectMapper objMapper = new ObjectMapper();
		try {
			param.setUser_id(new String().format("USER-%s", System.nanoTime()));

//			this.hashOp.put("user", "user_id", param.getUser_id());
//			this.hashOp.put("user", "user_name", param.getUser_name());
//			this.hashOp.put("user", "user_email", param.getUser_email());
			this.hashOp.put("users", param.getUser_id(), objMapper.writeValueAsString(param));
//			this.listOp.set("list_user", 1, objMapper.writeValueAsString(param));
//			this.listOp.set("list_user", 1, param);
			System.out.println("============================= PARAM HASH =================================");
			System.out.println(param.getUser_id());
			System.out.println(param.getUser_name());
			System.out.println(param.getUser_email());
			res = 100;
		}catch(Exception ex) {
			res= -1;
			ex.printStackTrace();
		}
		
		return res;
	}

	public String getStringUserTokenByKey(String key) {
		String result = "";
		Object dataUser = this.redisTemplate.opsForHash().get("token_users", key);
		if(dataUser != null) {
			result = this.redisTemplate.opsForHash().get("token_users", key).toString();
		}
		
		System.out.println("====================== GETTING USER'S TOKEN ======================");
		System.out.println(new String().format("key = %s", key));
		System.out.print("result = "); System.out.println(dataUser);
		
		return result;
	}
}
