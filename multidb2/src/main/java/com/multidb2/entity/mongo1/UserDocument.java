package com.multidb2.entity.mongo1;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("tb_user")
public class UserDocument {
	@Id
	private String id;
//	@Field("user_name")
	private String user_name;
//	@Field("user_email")
	private String user_email;
//	@Field("user_password")
	private String user_password;
	
	private String data;
	
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public UserDocument() {
		
	}
	
	public UserDocument(String name,String email,String password) {
		this.user_name = name;
		this.user_email = email;
		this.user_password = password;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getUser_email() {
		return user_email;
	}
	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}
	public String getUser_password() {
		return user_password;
	}
	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}
}
