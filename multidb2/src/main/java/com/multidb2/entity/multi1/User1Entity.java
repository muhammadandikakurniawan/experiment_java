package com.multidb2.entity.multi1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;

@Entity
@Table(name="tb_user",schema="public")
public class User1Entity {
	
	@Id()
	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	@Column(name="id")
	private Integer Id;
	
	@Column(name="data")
	private String Data;

	public User1Entity(String data) {
		this.Data = data;
	}
	
	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public String getData() {
		return Data;
	}

	public void setData(String data) {
		Data = data;
	}
	
	
	
}
