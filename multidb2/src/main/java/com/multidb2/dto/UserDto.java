package com.multidb2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDto {

	
	private String UserId;
	private String UserName;
	private String UserPassword;
	private String UserEmail;
	
	
	
	public String getUserId() {
		return UserId;
	}

	@JsonProperty(value="user_id")
	public void setUserId(String userId) {
		UserId = userId;
	}

	
	public String getUserName() {
		return UserName;
	}

	@JsonProperty(value="user_name",defaultValue="null_str")
	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getUserPassword() {
		return UserPassword;
	}

	@JsonProperty(value="user_password",defaultValue="null_str")
	public void setUserPassword(String userPassword) {
		UserPassword = userPassword;
	}

	public String getUserEmail() {
		return UserEmail;
	}

	@JsonProperty(value="user_email",defaultValue="null_str")
	public void setUserEmail(String userEmail) {
		UserEmail = userEmail;
	}
	
}
