package com.multidb2.util;

import java.util.Date;
import java.util.HashMap;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;

@Service()
public class JwtUtil {
	
	public JwtBuilder Builder(HashMap<String,Object> payload, String secretKey) {
		ObjectMapper objMapper = new ObjectMapper();
		
		Object result = null; 
		
		try {
			
			String subject = "";
			if(payload.get("subject") != null) {
				subject = payload.get("subject").toString();
			}
//			Long issueAt = 
			
			result =  Jwts.builder()
					.setClaims(payload)
					.setSubject(subject)
					.setIssuedAt(new Date(System.currentTimeMillis()))
					.signWith(SignatureAlgorithm.HS256, secretKey);
				
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return (JwtBuilder) result;
		
	}
	
	public HashMap<String,Object> getClaims(String token, String secretKey){
		HashMap<String,Object> res = new HashMap<String,Object>();
		Boolean isValid = true;
		Boolean isExpired = true;
		
		try {
			
		Jws jws = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);	
		
		res.put("payload",jws.getBody());
		res.put("header",jws.getHeader());
		res.put("signature",jws.getSignature());
	
		}
		catch(SignatureException ex) {
			isValid = false;
		}
		catch (ExpiredJwtException ex){
			isValid = false;
			isExpired = false;
	    }
		catch(Exception ex) {
			
		}
		
		res.put("isValid",isValid);
		res.put("isExpired",isExpired);
		
		return res;
	}
	
}
