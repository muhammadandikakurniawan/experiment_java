package CassandraDemo.Configs;

// import com.datastax.oss.driver.api.core.CqlSession;

// import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
// import org.springframework.context.annotation.Bean;
// import org.springframework.context.annotation.Configuration;
// import org.springframework.data.cassandra.config.AbstractCassandraConfiguration;
// import org.springframework.data.cassandra.config.CassandraSessionFactoryBean;
// import org.springframework.data.cassandra.config.SchemaAction;
// import org.springframework.data.cassandra.core.CassandraOperations;
// import org.springframework.data.cassandra.core.CassandraTemplate;
// import org.springframework.data.cassandra.core.convert.CassandraConverter;
// import org.springframework.data.cassandra.core.convert.MappingCassandraConverter;
// import org.springframework.data.cassandra.core.mapping.BasicCassandraMappingContext;
// import org.springframework.data.cassandra.core.mapping.CassandraMappingContext;
// import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;

// @Configuration
// @EnableCassandraRepositories(basePackages = {"CassandraDemo.Repositories"})
public class CassandraConfig{

    // @Override
    // protected String getKeyspaceName() {
    //     return "mukeyspace";
    // }

    // @Override
    // protected String getContactPoints(){
    //     return "mycassandra";
    // }

    // @Override
    // protected int getPort(){
    //     return 9042;
    // }

    // @Override
    // public SchemaAction getSchemaAction() {
    //     return SchemaAction.CREATE_IF_NOT_EXISTS;
    // }

    // @Override
    // public String[] getEntityBasePackages(){
    //     return new String[]{"CassandraDemo.Entities"};
    // }

    // @Bean
    // public CassandraMappingContext mappingContext() {
    //     return new BasicCassandraMappingContext();
    // }

    // @Bean
    // public CassandraConverter converter() {
    //     return new MappingCassandraConverter(mappingContext());
    // }

    // @Bean
    // public CassandraClusterFactoryBean cluster() {
    //     CassandraClusterFactoryBean cluster = new CassandraClusterFactoryBean();
    //     cluster.setContactPoints(getContactPoints());
    //     cluster.setPort(getPort());
    //     return cluster;
    // }

    // @Bean
    // public CassandraSessionFactoryBean session() throws Exception {
    //     CassandraSessionFactoryBean cassandraSessionFactoryBean = new CassandraSessionFactoryBean();
    //     cassandraSessionFactoryBean.setCluster(cluster().getObject());
    //     cassandraSessionFactoryBean.setKeyspaceName(getKeyspaceName());
    //     cassandraSessionFactoryBean.setConverter(converter());
    //     cassandraSessionFactoryBean.setSchemaAction(SchemaAction.NONE);
    //     return cassandraSessionFactoryBean;
    // }

    // @Bean
    // public CassandraOperations cassandraTemplate() throws Exception {
    //     return new CassandraTemplate(session().getObject());
    // }

    // @Bean
	// public CassandraOperations cassandraTemplate(CassandraSessionFactoryBean cassandraSession) {
	// 	return new CassandraTemplate(cassandraSession.getObject());
	// }
    
}
