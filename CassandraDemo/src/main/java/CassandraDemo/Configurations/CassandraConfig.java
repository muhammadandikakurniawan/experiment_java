package CassandraDemo.Configurations;

import java.util.Set;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.cassandra.config.AbstractCassandraConfiguration;
// import org.springframework.data.cassandra.config.CassandraClusterFactoryBean;
import org.springframework.data.cassandra.config.CassandraCqlSessionFactoryBean;
import org.springframework.data.cassandra.config.CassandraEntityClassScanner;
import org.springframework.data.cassandra.config.CassandraSessionFactoryBean;
// import org.springframework.data.cassandra.config.CassandraClusterFactoryBean;
import org.springframework.data.cassandra.config.SchemaAction;
import org.springframework.data.cassandra.core.CassandraAdminOperations;
import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.data.cassandra.core.CassandraTemplate;
import org.springframework.data.cassandra.core.convert.CassandraConverter;
import org.springframework.data.cassandra.core.convert.MappingCassandraConverter;
import org.springframework.data.cassandra.core.mapping.BasicCassandraMappingContext;
import org.springframework.data.cassandra.core.mapping.CassandraMappingContext;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;

@Configuration
@EnableCassandraRepositories(basePackages = "CassandraDemo.Repositories")
@EntityScan(basePackages = { "CassandraDemo.Entities" })
public class CassandraConfig 
// extends AbstractCassandraConfiguration 
{

    // @Bean
    // public CassandraClusterFactoryBean cluster() {
    //     CassandraClusterFactoryBean cluster = new CassandraClusterFactoryBean();
    //     cluster.setContactPoints("192.168.99.100");
    //     cluster.setPort(9042);
    //     return cluster;
    // }

    @Bean
    public CassandraMappingContext mappingContext() throws ClassNotFoundException {
        CassandraMappingContext mappingContext= new CassandraMappingContext();
        mappingContext.setInitialEntitySet(getInitialEntitySet());
        return mappingContext;
    }

    // @Override
    public String[] getEntityBasePackages() {
        return new String[]{"CassandraDemo.Entities"};
    }

    // @Override
    protected Set<Class<?>> getInitialEntitySet() throws ClassNotFoundException {
        return CassandraEntityClassScanner.scan(getEntityBasePackages());
    }

    @Bean
    public CassandraConverter converter() throws ClassNotFoundException {
        return new MappingCassandraConverter(mappingContext());
    }
    
    @Bean
    public CassandraSessionFactoryBean session() throws ClassNotFoundException {

        CassandraSessionFactoryBean session = new CassandraSessionFactoryBean();
        session.setKeyspaceName("mykeyspace");
        // session.setCluster(cluster().getObject());
        session.setContactPoints("192.168.99.100");
        session.setLocalDatacenter("datacenter1");
        session.setPort(9042);
        session.setConverter(converter());
        session.setSchemaAction(SchemaAction.CREATE_IF_NOT_EXISTS);
        return session;
    }

    @Bean
    public CassandraOperations cassandraTemplate() throws Exception {
        return new CassandraTemplate(session().getObject());
    }

    // @Override
    // protected String getKeyspaceName() {
    //     return "mykeyspace";
    // }

    // @Override
    // public String[] getEntityBasePackages() {
    //     return new String[]{"CassandraDemo.Entities"};
    // }

    // @Override
    // protected Set<Class<?>> getInitialEntitySet() throws ClassNotFoundException {
    //     return CassandraEntityClassScanner.scan(getEntityBasePackages());
    // }
}
