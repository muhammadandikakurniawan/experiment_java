package CassandraDemo.Entities;

import java.util.List;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;
import org.springframework.data.cassandra.repository.AllowFiltering;

import lombok.*;

@Table("Project")
@Setter
@Getter
public class Project {
    
    @PrimaryKey
    private String Id;

    @Column("Name")
    private String Name;

    @Column("Description")
    private String Description;

    @Column("Users")
    private List<String> Users;

}
