package CassandraDemo.Entities;

// import com.datastax.driver.mapping.annotations.Table;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

// import org.springframework.data.cassandra.mapping.PrimaryKey;
// import org.springframework.data.cassandra.mapping.Table;

import lombok.*;

@Getter
@Setter
@Table("product")
public class Product {
    
    @PrimaryKey
    private int Id;
    private String Name;
    // private int Price;
    
}
