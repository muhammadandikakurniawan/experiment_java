package CassandraDemo.Entities;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "User", forceQuote = true)
public class User {
    
    @PrimaryKey
    private String Id;

    private String Name;
    private String Email;
    private String Password;

}
