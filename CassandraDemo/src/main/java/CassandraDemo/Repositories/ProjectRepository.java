package CassandraDemo.Repositories;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import CassandraDemo.Entities.Project;

@Repository
public interface ProjectRepository extends CassandraRepository<Project,String>{
    
}
