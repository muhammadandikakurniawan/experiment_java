package CassandraDemo.Repositories;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import CassandraDemo.Entities.Product;

@Repository
public interface ProductRepository extends CassandraRepository<Product,Integer>{
    
}
