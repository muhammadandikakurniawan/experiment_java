package CassandraDemo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import CassandraDemo.Entities.Product;
import CassandraDemo.Entities.Project;
import CassandraDemo.Repositories.ProductRepository;
import CassandraDemo.Repositories.ProjectRepository;

@SpringBootTest
class CassandraDemoApplicationTests {

	@Autowired
	private ProductRepository _productRepo;

	@Autowired
	private ProjectRepository _projectRepo;

	@Test
	void contextLoads() {
	}

	@Test
	void insert_product_test() {
		Product data = new Product();
		data.setId(3);
		data.setName("ciki");

		Product insertRes = this._productRepo.save(data);

		String dbg = "";
	}

	@Test
	void get_product_test() {
		Product data = new Product();
		data.setId(3);
		data.setName("ciki");

		List<Product> insertRes = this._productRepo.findAll();

		String dbg = "";
	}

	@Test
	void insert_project_test() {
		Project data = new Project();
		List<String> users = new ArrayList<>();

		users.add(UUID.randomUUID().toString().replace('-', ' '));
		users.add(UUID.randomUUID().toString().replace('-', ' '));
		users.add(UUID.randomUUID().toString().replace('-', ' '));
		users.add(UUID.randomUUID().toString().replace('-', ' '));
		users.add(UUID.randomUUID().toString().replace('-', ' '));

		data.setId(UUID.randomUUID().toString().replace('-', ' '));
		data.setName("Ideas");
		data.setDescription("project for joining people");
		data.setUsers(users);

		Project insertRes = this._projectRepo.save(data);

		String dbg = "";
	}

}
