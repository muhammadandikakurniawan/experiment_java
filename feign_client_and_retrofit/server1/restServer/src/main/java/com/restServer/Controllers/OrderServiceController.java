package com.restServer.Controllers;

import com.restServer.Repository.OrderRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.*;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.restServer.Dto.*;

@RestController
@RequestMapping(value = "OrderService")
public class OrderServiceController {

    @Autowired
    private OrderRepo _orderRepo;

    @Autowired
    private HttpServletResponse _response;

    @Autowired
    private HttpServletRequest _request;

    @Autowired
    private ObjectMapper _objMapper;

    @RequestMapping(value = "GetOrderDetail", method = RequestMethod.POST/*
                                                                          * , produces =
                                                                          * MediaType.APPLICATION_STREAM_JSON_VALUE
                                                                          */)
    public Flux<OrderDto> GetOrderDetail() {
        System.out.println("============================== get all ===========================");
        return Mono.just(this._orderRepo.GetOrderDetail()).flatMapMany(el -> Flux.fromIterable(el));
    }

    @RequestMapping(value = "GetOrderDetailBySkuCode", method = RequestMethod.POST/*
                                                                                   * , produces = MediaType.
                                                                                   * APPLICATION_STREAM_JSON_VALUE
                                                                                   */)
    public Flux<OrderDto> GetOrderDetailBySkuCode(@RequestBody HashMap<String, Object> param) {
        String sku_code = param.get("sku_code").toString();
        System.out.println("============================== get by sku code ===========================");
        System.out.println(sku_code);
        System.out.println("==========================================================================");
        System.out.println(this._request.getHeader("client_name"));
        System.out.println(sku_code);
        String app_header = Optional.ofNullable(this._request.getHeader("app_header")).orElse("");
        if (!app_header.equals("")) {
            System.out.println("============================== get by sku code SUCCESS ===========================");
            return Mono.just(this._orderRepo.GetOrderDetailBySkuCode(sku_code))
                    .flatMapMany(el -> Flux.fromIterable(el));
        } else {
            this._response.setHeader("error_message", "app_header is not exist");
            this._response.setStatus(101);
            System.out.println("============================== get by sku code ERROR ===========================");
            System.out.println("app_header is not exist");
            return null;
        }
    }

    @RequestMapping(value = "GetOrderDetailBySkuCodeList", method = RequestMethod.POST)
    public List<OrderDto> GetOrderDetailBySkuCodeList(@RequestBody HashMap<String, Object> param) {
        String sku_code = param.get("sku_code").toString();
        System.out.println("============================== get by sku code ===========================");
        System.out.println(sku_code);
        System.out.println("==========================================================================");
        System.out.println(this._request.getHeader("client_name"));
        System.out.println(sku_code);
        String app_header = Optional.ofNullable(this._request.getHeader("app_header")).orElse("");
        if (!app_header.equals("")) {
            System.out.println("============================== get by sku code SUCCESS ===========================");
            var res = this._orderRepo.GetOrderDetailBySkuCode(sku_code);
            res.forEach(x -> {
                try {
                    System.out.println(this._objMapper.writeValueAsString(x));
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            });
            return res;
        }else{
            this._response.setHeader("error_message", "app_header is not exist");
            this._response.setStatus(101);
            System.out.println("============================== get by sku code ERROR ===========================");
            System.out.println("app_header is not exist");
            return null;
        }
    }
}
