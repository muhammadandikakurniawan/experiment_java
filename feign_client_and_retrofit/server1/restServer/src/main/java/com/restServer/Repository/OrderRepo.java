package com.restServer.Repository;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.restServer.Dto.OrderDto;
import com.restServer.Models.OrderModel;
import com.restServer.Models.ProductModel;
import com.restServer.Models.UserModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class OrderRepo {

    @Autowired
    private IOrderRepo _iOrderRepo;

    @PersistenceContext(name = "TrainingPU")
    private EntityManager _em;

    public List<OrderModel> GetAll(){
        return this._iOrderRepo.findAll();
    }

    public List<OrderDto> GetOrderDetail(){
        return this._em.createQuery(
            "SELECT new com.restServer.Dto.OrderDto(u, p) from OrderModel o "
            +"LEFT JOIN ProductModel p on p.SkuCode = o.SkuCode "
            +"LEFT JOIN UserModel u on u.Id = o.UserId"
        ).getResultList();
    }

    public List<OrderDto> GetOrderDetailBySkuCode(String skuCode){
        return this._em.createQuery(
            "SELECT new com.restServer.Dto.OrderDto(u, p) from OrderModel o "
            +"LEFT JOIN ProductModel p on p.SkuCode = o.SkuCode "
            +"LEFT JOIN UserModel u on u.Id = o.UserId "
            +"WHERE p.SkuCode = :sku_code"
        )
        .setParameter("sku_code", skuCode)
        .getResultList();
    }
    
}
