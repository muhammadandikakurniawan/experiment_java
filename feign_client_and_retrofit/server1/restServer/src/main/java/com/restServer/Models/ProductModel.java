package com.restServer.Models;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.*;

@Entity
@Table(name = "tb_product", schema = "training")
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    @JsonProperty("Id")
    private Integer Id;

    @Column(name="name")
    @JsonProperty("Name")
    private String Name;

    @Column(name="sku_code")
    @JsonProperty("SkuCode")
    private String SkuCode;

    @Column(name="price")
    @JsonProperty("Price")
    private String Price;
}
