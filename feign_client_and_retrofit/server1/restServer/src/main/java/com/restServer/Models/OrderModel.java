package com.restServer.Models;

import javax.persistence.*;

import lombok.*;

@Entity
@Table(name = "tb_order", schema = "training")
@Getter
@Setter
public class OrderModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer Id;

    @Column(name="sku_code")
    private String SkuCode;

    @Column(name="user_id")
    private Integer UserId;

	@OneToOne()
    @JoinColumns(value = {
        @JoinColumn(
            name = "sku_code", 
            referencedColumnName = "sku_code"
        ),
        @JoinColumn(
            name = "sku_code", 
            referencedColumnName = "sku_code", 
            insertable = false, 
            updatable = false
        )
    })
    private ProductModel Product;
    
}