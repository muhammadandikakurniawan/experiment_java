package com.restServer;

import java.sql.ResultSetMetaData;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@SpringBootTest
class RestServerApplicationTests {

	@Autowired
	private ObjectMapper _objMapper;

	@Test
	void contextLoads() {
	}

	@Test
	void JdbcTemplate_test() {

		String dbName = "db_training";
		String schName = "training";
		String tbName = "tb_product";

		DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setDriverClassName("org.postgresql.Driver");
		ds.setUrl(String.format("jdbc:postgresql://localhost:5432/%s", dbName));
		ds.setUsername("postgres");
		ds.setPassword("admin");
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(ds);

		List<HashMap<String,Object>> datas = jdbcTemplate.query(String.format("SELECT * FROM %s.%s", schName,tbName),
			new Object[] {}, 
			(data, rowNum) -> {
				return new HashMap<String,Object>(){{
					put("product_name", data.getString("name"));
					put("product_price", data.getString("price"));
					put("product_code", data.getString("sku_code"));
				}
			};
		});

		String dbg = "";
	}

	@Test
	void JdbcTemplate_test2() {

		String dbName = "db_training";
		String schName = "training";
		String tbName = "tb_order";

		DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setDriverClassName("org.postgresql.Driver");
		ds.setUrl(String.format("jdbc:postgresql://localhost:5432/%s", dbName));
		ds.setUsername("postgres");
		ds.setPassword("admin");
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(ds);

		List<HashMap<Object,Object>> datas = jdbcTemplate.query(String.format("SELECT * FROM %s.%s", schName,tbName),
			new Object[] {}, 
			(rs, rowNum) -> {

				HashMap<Object,Object> dataMapped = new HashMap<Object,Object>();
				
				ResultSetMetaData rsMd = rs.getMetaData();

				System.out.println("Number of column : "+rsMd.getColumnCount());

				for(int i = 1; i <= rsMd.getColumnCount(); i++){
					dataMapped.put(rsMd.getColumnName(i), rs.getObject(i));
				}

				return dataMapped;
			}
		);

		String dbg = "";
	}

}
