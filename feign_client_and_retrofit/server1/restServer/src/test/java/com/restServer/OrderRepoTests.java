package com.restServer;

import com.restServer.Repository.OrderRepo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import lombok.*;

@SpringBootTest
class OrderRepoTests {

	@Autowired
	private OrderRepo _orderRepo;

	@Test
	void contextLoads() {
	}

	@Test
	public void GetOrderDetail(){
		var res = this._orderRepo.GetOrderDetail();

		var dbg = "";
	}

	@Test
	public void GetOrderDetailBySkuCode(){
		var res = this._orderRepo.GetOrderDetailBySkuCode("P-001");
		var res2 = this._orderRepo.GetOrderDetail();
		
		var dbg = "";
	}

	@Test
	public void GetAll(){
		var res = this._orderRepo.GetAll();
		
		var dbg = "";
	}

}
