package com.feignClientTest;

import java.util.HashMap;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;

@SpringBootTest
public class HttpClientTest {

    @Test
	void contextLoads() {

    }

    @Test
	void getdataTest() {
        RestTemplate restTemplate = new RestTemplate();

        HashMap<String,Object> dataBody = new HashMap();
        dataBody.put("sku_code", "P-001");

        HttpHeaders headers = new HttpHeaders();
        headers.add("app_header", "123345345");

        HttpEntity httpEntity = new HttpEntity<HashMap>(dataBody,headers);
    }
    
}
