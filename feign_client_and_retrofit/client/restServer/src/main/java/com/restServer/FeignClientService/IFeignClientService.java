package com.restServer.FeignClientService;

import java.util.HashMap;
import java.util.List;

import com.restServer.Dto.OrderDto;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public interface IFeignClientService {
    
    @RequestMapping(value = "OrderService/GetOrderDetail", method = RequestMethod.POST)
    public List<OrderDto> GetOrderDetail();

    @RequestMapping(value = "OrderService/GetOrderDetailBySkuCode", method = RequestMethod.POST)
    public List<OrderDto> GetOrderDetailBySkuCode(HashMap<String,Object> param);

}
