package com.restServer.Controllers;

import java.util.Arrays;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping(value = "ClientService")
public class ClientServiceController {

    @Autowired
    private RestTemplate _restTemplate;

    private String url1 = "http://localhost:1000/OrderService/GetOrderDetail";
    private String url2 = "http://localhost:2000/OrderService/GetOrderDetail";

    @RequestMapping(value = "GetOrderDetail", method = RequestMethod.GET)
    public Object GetOrderDetail() {
        try{
            HashMap<String, Object> dataBody = new HashMap();
            dataBody.put("sku_code", "P-001");

            HttpHeaders headers = new HttpHeaders();
            headers.add("app_header", "123345345");
            headers.add("client_name", "test rest");

            HttpEntity httpEntity = new HttpEntity<HashMap>(dataBody, headers);
            
            ResponseEntity<Object[]> res = this._restTemplate.exchange(this.url1, HttpMethod.POST, httpEntity,
                    Object[].class);

            return res.getBody();
        }catch(Exception ex){
            return ex.getMessage();
        }
    }

}
