package com.restServer.Dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.restServer.Models.*;

import lombok.*;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderDto {
    
    public OrderDto(UserModel u, ProductModel p) {
        this.User = u;
        this.Product = p;
    }

    public OrderDto(){}

    @JsonProperty("User")
    private UserModel User;
    @JsonProperty("Product")
    private ProductModel Product;
}
