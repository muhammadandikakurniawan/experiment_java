package com.restServer.Proxy;

import com.restServer.Configs.FeignClientConfig;
import com.restServer.FeignClientService.IFeignClientService;
import com.restServer.FeignFallback.FeignServiceFallback;

import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name="feign-client", fallback = FeignServiceFallback.class)
public interface IFeignClientProxy extends IFeignClientService{
    
}
