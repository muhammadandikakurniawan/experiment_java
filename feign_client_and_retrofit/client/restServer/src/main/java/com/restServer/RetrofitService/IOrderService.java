package com.restServer.RetrofitService;

import java.util.HashMap;
import java.util.List;

import com.restServer.Dto.OrderDto;

import reactor.core.publisher.Flux;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface IOrderService {

    @Headers({
        "client_name:dika",
        "app_header:header-dika",
    })
    @POST("/OrderService/GetOrderDetailBySkuCode")
    Call<List<OrderDto>> GetOderDetailBySkuCode(@Body HashMap<String,Object> request);
    // Call<Flux<OrderDto>> GetOderDetailBySkuCode(@Body HashMap<String,Object> request, @Header("app_name") String app_name, @Header("app_header") String app_header);
    

    @Headers({
        "client_name:dika",
        "app_header:header-dika",
    })
    @POST("/OrderService/GetOrderDetailBySkuCodeList")
    Call<List<OrderDto>> GetOrderDetailBySkuCodeList(@Body HashMap<String,Object> request);
    // Call<Flux<OrderDto>> GetOderDetailBySkuCode(@Body HashMap<String,Object> request, @Header("app_name") String app_name, @Header("app_header") String app_header);
    
}
