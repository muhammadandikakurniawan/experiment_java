package com.restServer.FeignFallback;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.restServer.Dto.OrderDto;
import com.restServer.Proxy.IFeignClientProxy;

import org.springframework.stereotype.Component;

@Component
public class FeignServiceFallback implements IFeignClientProxy {

    @Override
    public List<OrderDto> GetOrderDetail() {
        return Arrays.asList();
    }

    @Override
    public List<OrderDto> GetOrderDetailBySkuCode(HashMap<String, Object> param) {
        return Arrays.asList();
    }
    
}
