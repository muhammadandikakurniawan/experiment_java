package com.restServer.RetrofitService;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.restServer.Dto.OrderDto;

import org.springframework.stereotype.Service;

import reactor.core.publisher.Flux;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

@Service
public class OrderService {

    private Retrofit _retrofit;

    public OrderService() {
        this._retrofit = new Retrofit.Builder().baseUrl("http://localhost:1000")
                .addConverterFactory(JacksonConverterFactory.create()).build();
    }

    public void GetOderDetailBySkuCode_test() throws IOException {
        IOrderService service = this._retrofit.create(IOrderService.class);

        HashMap<String,Object> reqBody = new HashMap<String,Object>();
        reqBody.put("sku_code", "P-001");

        // HashMap<String,String> headers = new HashMap<String,String>();
        // headers.put("client_name", "dika");
        // headers.put("app_header", "app_header_dika");

        var callFlux = service.GetOderDetailBySkuCode(reqBody);
        // Call<Flux<OrderDto>> call = service.GetOderDetailBySkuCode(reqBody,headers.get("client_name"),headers.get("app_header"));
        List<OrderDto> resultRaw = Arrays.asList();

        var resFlux = callFlux.execute().body();

        // resFlux.subscribe(x -> {
        //     resultRaw.add(x);
        // });

        var call = service.GetOrderDetailBySkuCodeList(reqBody);

        var res = call.execute().body();

        var dbg = "";
    }

}
