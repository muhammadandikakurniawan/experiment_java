package com.restServer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.criteria.Order;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.restServer.Dto.OrderDto;
import com.restServer.RetrofitService.OrderService;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import reactor.core.publisher.Flux;

@SpringBootTest
public class HttpClientTest {

    @Autowired
    private ObjectMapper _objMapper;
    @Autowired
    private OrderService _retrofit_orderService;

    @Test
    void contextLoads() {

    }

    @Test
    void getdataTest() {
        RestTemplate restTemplate = new RestTemplate();

        HashMap<String, Object> dataBody = new HashMap();
        dataBody.put("sku_code", "P-001");

        HttpHeaders headers = new HttpHeaders();
        headers.add("app_header", "123345345");
        headers.add("client_name", "test rest");

        HttpEntity httpEntity = new HttpEntity<HashMap>(dataBody, headers);

        ResponseEntity<List> res = restTemplate.exchange("http://localhost:2000/OrderService/GetOrderDetail",
                HttpMethod.POST, httpEntity, List.class);

        List<OrderDto> datas = new ArrayList();
        res.getBody().stream().forEach(x -> {
            datas.add(this._objMapper.convertValue(x, OrderDto.class));
        });

        // List<OrderDto> resBody = (List<OrderDto>) res.getBody();

        var dbg = "";
    }

    @Test
    public void Retrofit_test() throws IOException {
        this._retrofit_orderService.GetOderDetailBySkuCode_test();
    }
    
}
