package com.restServer.Models;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.*;

@Entity
@Table(name = "tb_user", schema = "training")
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    @JsonProperty("Id")
    private Integer Id;

    @Column(name="name")
    @JsonProperty("Name")
    private String Name;

    @Column(name="email")
    @JsonProperty("Email")
    private String Email;

    @Column(name="password")
    @JsonProperty("Password")
    private String Password;

}
