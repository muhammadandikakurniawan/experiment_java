package com.multidb.repository.mongo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.multidb.entity.mongo.*;

@Repository
public interface UserRepository extends MongoRepository<UserEntity,Integer>{

}
