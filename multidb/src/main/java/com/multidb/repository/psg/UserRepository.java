package com.multidb.repository.psg;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.multidb.entity.psg.*;

@Repository
public interface UserRepository extends JpaRepository<UserEntity,Integer>{

}
