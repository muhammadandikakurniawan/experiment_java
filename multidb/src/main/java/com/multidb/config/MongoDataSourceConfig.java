package com.multidb.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;

import static java.util.Collections.singletonMap;

@Configuration
@EnableMongoRepositories(basePackages = { "com.multidb.repository.mongo" })
@EnableAutoConfiguration
public class MongoDataSourceConfig extends AbstractMongoConfiguration {
	 
    @Override
    protected String getDatabaseName() {
        return "latihan";
    }
  
    @Override
    protected String getMappingBasePackage() {
        return "com.multidb.entity.mongo";
    }
    
    @Bean
    public MongoTemplate mongoTemplate() throws Exception {
        return new MongoTemplate(mongoClient(), getDatabaseName());
    }
    
	@Override
	@Bean
	public MongoClient mongoClient() {
	   return new MongoClient("localhost:27017");
	}
	 
	 
	 //========================================================================================
//	 @Primary
//	    @Bean(name = "firstEntityManagerFactory")
//	    public LocalContainerEntityManagerFactoryBean firstEntityManagerFactory(final EntityManagerFactoryBuilder builder,
//	                                                                            final @Qualifier("first-db") DataSource dataSource) {
//	        return builder
//	                .dataSource(dataSource)
//	                .packages("com.marcosbarbero.wd.pcf.multidatasources.first.domain")
//	                .persistenceUnit("firstDb")
//	                .properties(singletonMap("hibernate.hbm2ddl.auto", "create-drop"))
//	                .build();
//	    }
//
//	    @Primary
//	    @Bean(name = "firstTransactionManager")
//	    public PlatformTransactionManager firstTransactionManager(@Qualifier("firstEntityManagerFactory")
//	                                                              EntityManagerFactory firstEntityManagerFactory) {
//	        return new JpaTransactionManager(firstEntityManagerFactory);
//	    }
	
//	@Bean(name = "MongoDataSource")
//	@ConfigurationProperties(prefix = "spring.mongodb.datasource")
//	public DataSource dataSource() {
//		return DataSourceBuilder.create().build();
//	}
	
//	@Bean(name = "MongoDataSource")
//	@ConfigurationProperties(prefix = "spring.mongodb.datasource")
//	public Mongo dataSource() {
//		
////		#mongodb
////		spring.mongodb.datasource.host=localhost
////		spring.mongodb.datasource.port=27017
////		spring.mongodb.datasource.database=latihan
//		return new MongoClient("localhost:27017");
//	}
	
//	  @Bean(name = "MongoEntityManagerFactory")
//	  public LocalContainerEntityManagerFactoryBean entityManagerFactory(EntityManagerFactoryBuilder builder,final @Qualifier("MongoDataSource") DataSource dataSource) {
//	    return
//	      builder
//	        .dataSource(dataSource)
//	        .packages("com.multidb.entity.mongo")
//	        .persistenceUnit("mongo")
//	        .build();
//	  }
}
