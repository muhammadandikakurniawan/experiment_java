package com.multidb.config;


import java.util.HashMap;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableAutoConfiguration
@EnableJpaRepositories(entityManagerFactoryRef = "PsgEntityManagerFactory",transactionManagerRef = "PsgTransactionManager",basePackages = { "com.multidb.repository.psg" })
@EntityScan("com.multidb.entity.psg")
public class PsgSqlDataSourceConfig {
	 
	@Primary
	@Bean(name = "PsgDataSource")
//	@ConfigurationProperties(prefix = "spring.datasource")
	public DataSource dataSource() {
		return DataSourceBuilder
			   .create()
			   .url("jdbc:postgresql://localhost:5432/latihan")
			   .username("andi")
			   .password("humorismenalws06")
			   .driverClassName("org.postgresql.Driver")
			   .build();
	}
			
	@Primary
	@Bean(name = "PsgEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(EntityManagerFactoryBuilder builder, @Qualifier("PsgDataSource") DataSource dataSource) 
	{
//		HashMap<String,Object> properties = new HashMap<>();
//		properties.put("hibernate.hbm2ddl.auto","update");
//		properties.put("hibernate.dialect","org.hibernate.dialect.PostgreSQLDialect");
	    return builder.dataSource(dataSource).packages("com.multidb.entity.psg").persistenceUnit("psg").build();
	}  
		
	@Primary
	@Bean(name = "PsgtransactionManager")
    public PlatformTransactionManager transactionManager(@Qualifier("PsgEntityManagerFactory") EntityManagerFactory entityManagerFactory) 
	{
      return new JpaTransactionManager(entityManagerFactory);
    }
	
}
