package com.JavaService.m1_java_springboot.filter;

import java.io.IOException;

import javax.naming.AuthenticationException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.*;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

@WebFilter(urlPatterns = "/m1_java_springboot/*")
public class AuthFilter implements Filter{
	
	@Override
	public void doFilter(ServletRequest request,ServletResponse response,FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httprequest = (HttpServletRequest) request;
		HttpServletResponse httpresponse = (HttpServletResponse) response;
		String Authorization = httprequest.getHeader("Authorization");

		System.out.println("=============================== RUN FILTER ===================================");
		System.out.println(httprequest.getParameter("file"));
		System.out.println("authorization (auth filter) : "+Authorization);
		String reqUrl = "http://"+httprequest.getServerName()+httprequest.getRequestURI();
		System.out.println("endpoint : "+reqUrl );
		
		try {
			if(Authorization != null || Authorization != "") {
				System.out.println("auth is empty : "+(Authorization == ""));
				System.out.println("auth = basic 123 : "+(Authorization.equalsIgnoreCase("basic 123")));
				if(Authorization.equalsIgnoreCase("basic 123")) {
					System.out.println("=============================== AUTH OK ===================================");
				}
				else {
					System.out.println("Authorization is not valid harusnya keredirect");
//					httpresponse.sendRedirect("/redirect/unauthorize");
//					httpresponse.sendRedirect("http://localhost:8885/redirect/unauthorize");
				}
			}
			else {
				System.out.println("UnAuthorized");
//				httpresponse.sendRedirect("/redirect/unauthorize");
//				httpresponse.sendRedirect("http://localhost:8885/redirect/unauthorize");
			}
			
		}
		catch(NullPointerException ex) {
//			httpresponse.sendError(httpresponse.SC_UNAUTHORIZED);
//			httpresponse.sendRedirect("/redirect/unauthorize");
			System.out.println("====================== error ======================");
			System.out.println(ex.getMessage());
			System.out.println("====================== error ======================");
		} 
		catch(Exception ex) {
//			httpresponse.sendError(httpresponse.SC_UNAUTHORIZED);
//			httpresponse.sendRedirect("/redirect/unauthorize");
			System.out.println("====================== error ======================");
			System.out.println(ex.getMessage());
			System.out.println("====================== error ======================");
		} 
//		finally {
//			chain.doFilter(request, response);
//		}
		chain.doFilter(request, response);
		
	}
}
