package com.JavaService.m1_java_springboot.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import org.apache.tomcat.util.codec.binary.Base64;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.github.junrar.Archive;
import com.github.junrar.exception.RarException;
import com.github.junrar.extract.ExtractArchive;
import com.github.junrar.rarfile.FileHeader;
import com.google.common.io.ByteStreams;

import com.testautomationguru.utility.PDFUtil;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import com.lowagie.text.pdf.PdfWriter;

import java.awt.*;
import java.awt.image.BufferedImage;



@RestController
@CrossOrigin
@RequestMapping("/m1_java_springboot/filemanage")
public class FileManageController {
	
	@Autowired
	private Environment env;
	
	@Value("${file.upload-dir}")
	private String uploadDir;
	
	@RequestMapping("/base64upload")
	public HashMap<String,Object> Base64Save(@RequestBody HashMap<String,Object> params) throws IOException{
		HashMap<String,Object> res = new HashMap<String,Object>(){};
		
		try {
			String base64FileParam = (String) params.get("file");
			
			byte[] byteBase64File = Base64.decodeBase64(base64FileParam);
			InputStream inputStrm = new ByteArrayInputStream(byteBase64File);
			
			String exFile = URLConnection.guessContentTypeFromStream(inputStrm).split("/")[1];
			Date date = new Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy-HH:mm:ss.SSS");
			
			FileOutputStream fout = new FileOutputStream(this.uploadDir+date.getTime()+"."+exFile);
			fout.write(byteBase64File);
			
			res.put("ex_file",exFile);
			
			System.out.println("================================== END POINT PROCESS ==================================");
//			System.out.println("Params = "+params);
			System.out.println("file_process = "+URLConnection.guessContentTypeFromStream(inputStrm));
			System.out.println("stream = "+inputStrm);
			System.out.println("date = "+date.getTime());
			System.out.println("date_format = "+dateFormat.toString());
			System.out.println("date_and_format = "+dateFormat.toString());
			System.out.println("new_file_name = "+this.uploadDir+date.getTime()+"."+exFile);
			
			res.put("status","200");
			res.put("message","save file successful");
		}catch(Exception ex) {
			res.put("status","500");
			res.put("message",ex.getMessage());
		}
		System.out.println(res);
		return res;
	}
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public HashMap<String,Object> SaveFile(@RequestParam("file") MultipartFile file){
		HashMap<String,Object> result = new HashMap<String,Object>();
		
		try {
			
			//cara 1
			long startTime1 = System.nanoTime();

			byte[] fileByte = file.getBytes();
			
			long endTime1 = System.nanoTime();
			
			long processTime1 = endTime1 - startTime1;
			
			
			//cara2
			long startTime2 = System.nanoTime();
			
			InputStream fileInputStream2 = file.getInputStream();
			BufferedInputStream bufferInputStream2 = new BufferedInputStream(fileInputStream2);
			byte[] fileByte2 = ByteStreams.toByteArray(bufferInputStream2);
			
			long endTime2 = System.nanoTime();
			
			long processTime2 = endTime2 - startTime2;
			
			result.put("data1", Base64.encodeBase64String(fileByte));
			result.put("process_time1", processTime1);
			
			File f = new File(this.env.getProperty("file.upload-dir")+file.getOriginalFilename());
			
			
			if(f.exists()) {
				result.put("status", "file has been exists");
			}else if(!f.exists()) {
				FileOutputStream fout = new FileOutputStream(this.env.getProperty("file.upload-dir")+file.getOriginalFilename());
				fout.write(fileByte2);
			}
			
			
			result.put("data2", Base64.encodeBase64String(fileByte2));
			result.put("process_time2", processTime2);
			System.out.println("=================== encode 1 ===================");
			System.out.println(Base64.encodeBase64String(fileByte));
			System.out.println("=================== encode 2 ===================");
			System.out.println(Base64.encodeBase64String(fileByte2));
			System.out.println("file name = "+file.getOriginalFilename());

		}
		catch(Exception e) {
			result.put("error", e.getMessage());
		}
		
		return result;
	}
	
	@RequestMapping(value="/read",method=RequestMethod.GET)
	public ArrayList<HashMap<String,Object>> readFile(){
		ArrayList<HashMap<String,Object>> res = new ArrayList<>();
		
		String path = this.env.getProperty("file.upload-dir");
		File folder = new File(path);
		
		for(File f : folder.listFiles()) {
			res.add(
					new HashMap<String,Object>(){
						{
							put("file_size",+f.length());
							put("file_name",f.getName());
							put("file_path","192.168.3.248/files/"+f.getName());
						}
					}
					);
		}
		
		return res;
	}
	
	@RequestMapping(value="/multifile",method=RequestMethod.POST)
	public HashMap<String,Object> SaveMultiFile(@RequestParam("file") MultipartFile[] file){
		HashMap<String,Object> result = new HashMap<String,Object>();
		
		try {
			System.out.println("============================================");
			for(int i = 0; i < file.length; i++) {
				System.out.println(file[i].getOriginalFilename());
			}
			System.out.println("============================================");
			result.put("status", "ok");
		}
		catch(Exception e) {
			result.put("error", e.getMessage());
		}
		
		return result;
	}
	
	@RequestMapping(value="/readjsonfile",method=RequestMethod.POST)
	public HashMap<String,Object> readJsonFile(@RequestParam("file") MultipartFile param) throws IOException{
		HashMap<String,Object> res = new HashMap<String,Object>();
		
		Long bufferStart = System.nanoTime();
//		ByteArrayInputStream streamFile = new ByteArrayInputStream(param.getBytes());
		BufferedInputStream bufferParam = new BufferedInputStream(param.getInputStream());
		String readJsonFileBuffer = new String(ByteStreams.toByteArray(bufferParam));
		Long bufferTime = System.nanoTime() - bufferStart;
		
		Long radStart = System.nanoTime();
		String readJsonFile = new String(param.getBytes());
		Long readTime = System.nanoTime() - radStart;
		
//		Runtime.getRuntime().exec("cls");
//		System.out.print("\033\143");
		String os = System.getProperty("os.name");
		
		System.out.println("============================================== READ JSON FILE ==============================================");
		System.out.println(readJsonFile);
		System.out.println("Time buffer= "+bufferTime);
		System.out.println("============================================= READ BUFFER FILE =============================================");
		System.out.println(readJsonFile);
		System.out.println("Time without buffer= "+readTime);
		System.out.println("============================================================================================================");
		System.out.println("OS = "+os);
		
		return res;
	}
	
	@RequestMapping(value="/unzipfile",method=RequestMethod.POST)
	public HashMap<String,Object> unzipFile(@RequestParam("file") MultipartFile param) throws IOException, RarException{
		System.out.println("=================================== UNZIP CONTROLLER ===================================");
		HashMap<String,Object> res = new HashMap<String,Object>();
		String pathZipFile = "D:\\programming\\project\\microservices\\sit\\m1_java_springboot\\src\\files\\zipFile";
		String ExtFile = param.getOriginalFilename().split("\\.")[1];
		String newFileZipName = (new Date()).getTime()+"";
//		System.out.println("temporary file name = "+fileTmpName);
//========================== save file from param multipartfile ==========================
		BufferedInputStream buffIPS = new BufferedInputStream(param.getInputStream());
		FileOutputStream fOPS = new FileOutputStream(pathZipFile+"\\zip\\"+newFileZipName+"."+ExtFile);
		fOPS.write(ByteStreams.toByteArray(buffIPS));
//		BufferedOutputStream buffOPS = new BufferedOutputStream(fOPS);
//		buffOPS.write(ByteStreams.toByteArray(buffIPS));
//========================================================================================
		
//		byte[] buffer = param.getBytes();
//		
////		BufferedInputStream bufferStream = new BufferedInputStream(param.getInputStream());
//		
//		File f = new File(pathZipFile+"\\data.rar");
//		FileInputStream fis = new FileInputStream(f);
//		
//		System.out.println("fis = ");
//		System.out.println(fis);
//		
//		System.out.println("file");
//		System.out.println(f);
//		
//		ZipInputStream zis = new ZipInputStream(fis);
//		ZipEntry ze = zis.getNextEntry();
//		
////		System.out.println(new FileInputStream(new File(param.getOriginalFilename())));
//		
//		System.out.println("ZE");
//		System.out.println(ze);
//		
//		while(ze != null) {
//			File newFile = new File(pathZipFile,"data.json");
//			System.out.println(newFile.getAbsolutePath());
//			//make dir for unzipped data
////			new File(newFile.getParent()).mkdirs();
//			
//			FileOutputStream fos = new FileOutputStream(newFile);
//			
//			Integer len;
//			
//			while((len = zis.read(buffer))>0) {
//				fos.write(buffer,0,len);
//			}
//			fos.close();
//			ze = zis.getNextEntry();
//		}
//		
//		zis.close();
//		zis.closeEntry();
		
		File rarFile = new File(pathZipFile+"\\zip\\"+newFileZipName+"."+ExtFile);
		Archive archive = new Archive(rarFile);
		FileHeader fh = archive.nextFileHeader();
		String extUnzipFile = fh.getFileNameString().trim().split("\\.")[1];
	    ExtractArchive extractArchive = new ExtractArchive();
	    System.out.println(fh.getFileNameString());
        while(fh!=null){        
            File fileEntry = new File(pathZipFile+"\\unzip\\"+newFileZipName+"."+extUnzipFile);
            System.out.println(fileEntry.getAbsolutePath());
            FileOutputStream os = new FileOutputStream(fileEntry);
            archive.extractFile(fh, os);
            os.close();
            fh=archive.nextFileHeader();
        }
	    System.out.println("finished.");
	    System.out.println("finished. = "+extUnzipFile);
		
		System.out.println("================================================================================");
		
		return res;
	}
	
	@RequestMapping(value="/compresssize",method=RequestMethod.POST)
	public HashMap<String,Object> CompressSize(@RequestParam("file") MultipartFile param) throws IOException, DocumentException{
		HashMap<String,Object> res = new HashMap<String,Object>();
		
		String extParam = param.getContentType().split("/")[1];
		
		String dstPath = "D:\\programming\\project\\microservices\\sit\\m1_java_springboot\\src\\files\\compress\\compress_"+param.getOriginalFilename();
		String dstPathUtil1 = "D:\\programming\\project\\microservices\\sit\\m1_java_springboot\\src\\files\\compress\\compress_util_1"+param.getOriginalFilename();
		String dstPathUtil2 = "D:\\programming\\project\\microservices\\sit\\m1_java_springboot\\src\\files\\compress\\compress_util_2"+param.getOriginalFilename();
//		FileOutputStream fos = new FileOutputStream(dstPath);
//		FileInputStream fis = (FileInputStream) param.getInputStream();
//	      ====================================================================================================
		byte[] bufferFile = param.getBytes();
		BufferedOutputStream buffOs = new BufferedOutputStream(new FileOutputStream(dstPath));
		buffOs.write(bufferFile,0,bufferFile.length);
		
		System.out.println("=================================== COMPRESS CONTROLLER ===================================");
		if(extParam.equalsIgnoreCase("pdf")) {
			System.out.println("=================================== PDF ===================================");
			PdfReader reader = new PdfReader(dstPath);
			PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dstPathUtil1),PdfWriter.VERSION_1_5);
			stamper.getWriter().setCompressionLevel(9);
			stamper.setFullCompression();
		    stamper.close();
		    
		}else {
			System.out.println("=================================== IMAGE ===================================");
			BufferedImage buffImgOri = ImageIO.read(new File(dstPath));
			
			FileOutputStream out = new FileOutputStream(dstPathUtil1);
			
			ImageWriter imgWriter = ImageIO.getImageWritersByFormatName(extParam).next(); 
			ImageOutputStream ios = ImageIO.createImageOutputStream(out);
			imgWriter.setOutput(ios);
			
	        ImageWriteParam imgWriterParam = imgWriter.getDefaultWriteParam();
	        if (imgWriterParam.canWriteCompressed()){
	        	imgWriterParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
	        	imgWriterParam.setCompressionQuality(0.50f);
	        }

	        imgWriter.write(null, new IIOImage(buffImgOri, null, null), imgWriterParam);

	        out.close();
	        ios.close();
	        imgWriter.dispose();
		}
	    
	    File file_ori = new File(dstPath);
	    File file_compress = new File(dstPathUtil1);
	    
	    System.out.println("original size   = "+file_ori.length());
	    System.out.println("compressed size = "+file_compress.length());
	    System.out.println("percentage_reducing_size = "+(100-((new Float(file_compress.length())/new Float(file_ori.length()))*100))+"%");
		
	    System.out.println("FILE NAME = "+param.getOriginalFilename().toString());
		System.out.println("===========================================================================================");
		res.put("file_name", param.getOriginalFilename());
		res.put("size_before", param.getSize());
		return res;
	}
	
	  private static void showFileSize(String filename) throws IOException {
		    PdfReader reader = new PdfReader(filename);
		    System.out.print("Size ");
		    System.out.print(filename);
		    System.out.print(": ");
		    System.out.println(reader.getFileLength());
		  }
	

}
