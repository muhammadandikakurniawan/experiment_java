package com.JavaService.m1_java_springboot.controller;

import java.util.HashMap;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/redirect")
public class RedirectController {

	@RequestMapping("/unauthorize")
	public HashMap<String,Object> unauth(){
		HashMap<String,Object> res = new HashMap<>();
		res.put("errorcode","401");
		res.put("message","token is not valid");
		System.out.println("============================== redirect ===========================");
		System.out.println(res);
		return res;
	}
	
}
