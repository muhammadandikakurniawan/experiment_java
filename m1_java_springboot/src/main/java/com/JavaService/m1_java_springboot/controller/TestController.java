package com.JavaService.m1_java_springboot.controller;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.ByteStreams;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
@CrossOrigin
@RequestMapping("/test")
public class TestController {
	
	@Autowired
	private Environment env;
	
	@Autowired
//	private ConfigurationModel configModel;
	
	@RequestMapping(value="/envvar",method=RequestMethod.GET)
	public HashMap<String,Object> EnviVar(){
		HashMap<String,Object> res = new HashMap<String,Object>();
		
//		String env_name = this.env.getProperty("svr.name");
		
//		res.put("env_name",env_name);
//		res.put("env_wind",this.configModel.getServiceName());
		
		return res;
	}
	
	@RequestMapping(value="testjson",method=RequestMethod.POST)
	@ApiResponses(value= {
			@ApiResponse(code=200,message="success"),
	        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
	        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 400, message = "please give me valid request")
	})
	public Object[] testJson(@RequestBody String params) throws IOException{
		ObjectMapper objMapper = new ObjectMapper();
		
		Long startBuffer = System.nanoTime();
		InputStream paramsStream = new ByteArrayInputStream(params.getBytes(Charset.forName("UTF-8")));
		BufferedInputStream buffer = new BufferedInputStream(paramsStream);
		Object[] res= objMapper.readValue(params, Object[].class);
		String paramBuffer = new String(ByteStreams.toByteArray(buffer));
		Long endBuffer = System.nanoTime();
		Long processTimeBuffer = endBuffer - startBuffer;
		
		Long startDirect = System.nanoTime();
		String paramDirect = params;
		Long endDirect = System.nanoTime();
		Long processTimeDirect = endDirect - startDirect;
		
		System.out.println("================================= buffer ==================================");
		System.out.println("process time = "+processTimeBuffer);
		System.out.println("start time = "+startBuffer);
		System.out.println("end time = "+endBuffer);
//		System.out.println(paramBuffer);
		System.out.println("====================================================================");
		System.out.println("================================= direct ==================================");
		System.out.println("process time = "+processTimeDirect);
		System.out.println("start time = "+startDirect);
		System.out.println("end time = "+endDirect);
//		System.out.println(paramDirect);
		System.out.println("====================================================================");
		
//		System.out.println(((List)((HashMap<String,Object>)res.get("data1")).get("data11")).get(1));
		
		return res;
	}
	
	
	
	
}
