package com.demo.Configs;

import com.demo.Filters.AuthInterceptor;
import com.demo.Services.AuthService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InterceptorConfig implements WebMvcConfigurer{
    
    @Autowired
    private AuthService _authService;
    
    @Override
    public void addInterceptors(InterceptorRegistry reg){
        reg.addInterceptor(new AuthInterceptor(_authService)).addPathPatterns("/Data");
    }

}
