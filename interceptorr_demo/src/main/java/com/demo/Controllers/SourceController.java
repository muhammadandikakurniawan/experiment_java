package com.demo.Controllers;

import java.util.HashMap;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SourceController {
    
    @RequestMapping(value = "Data",method = {RequestMethod.POST,RequestMethod.GET})
    public HashMap<String,Object> Data(){
        return new HashMap<String,Object>(){{
            put("status", "success");
        }};
    }

    @RequestMapping(value = "Free",method = {RequestMethod.POST,RequestMethod.GET})
    public HashMap<String,Object> Free(){
        return new HashMap<String,Object>(){{
            put("status", "success");
        }};
    }

}
