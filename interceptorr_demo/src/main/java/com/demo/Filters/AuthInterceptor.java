package com.demo.Filters;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import com.demo.Services.AuthService;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.ContentCachingResponseWrapper;

import org.apache.commons.io.IOUtils;

@Component
public class AuthInterceptor extends HandlerInterceptorAdapter{

    private AuthService _authService;
    private ObjectMapper _objectMapper;

    public AuthInterceptor(){}
    public AuthInterceptor(AuthService authService){
        this._authService = authService;
        this._objectMapper = new ObjectMapper();
    }

    @Override
    public boolean preHandle(HttpServletRequest requestServlet, HttpServletResponse responseServlet, Object handler) throws Exception
    {
        System.out.println("MINIMAL: INTERCEPTOR PREHANDLE CALLED");
        Boolean validRes = _authService.IsValidRequest(requestServlet);
        if(!validRes){
            HashMap<String, Object> resBody = new HashMap<String, Object>(){{
                put("status", "401");
                put("message", "not authorize");
            }};
            responseServlet.getWriter().write(this._objectMapper.writeValueAsString(resBody));
            responseServlet.setContentType("application/json");
        }
        return validRes;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception
    {
        System.out.println("MINIMAL: INTERCEPTOR POSTHANDLE CALLED");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception exception) throws Exception
    {
        System.out.println("MINIMAL: INTERCEPTOR AFTERCOMPLETION CALLED");
        System.out.println(request.getRequestURI());
        // IOUtils.toString(request.getInputStream(),StandardCharsets.UTF_8);
        
    }
}
