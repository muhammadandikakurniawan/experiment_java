package com.demo.Services;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

@Service
public class AuthService {
    
    public Boolean IsValidRequest(HttpServletRequest req){
        
        return req.getHeader("auth") != null;

    }

}
