package com.AsyncDemo;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.Stack;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.AsyncDemo.Models.StudentModel;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class AsyncDemoApplicationTests {

	private PriorityQueue<StudentModel> pq_student = new PriorityQueue<>();

	@Test
	void contextLoads() {
		List<String> ls = new ArrayList();
		ls.add("str1");
		ls.add("str1");
		ls.add(null);
		String row;
		
		for(String l : ls){
			System.out.println((row = l) != null);
		}

	}

	@Test
	void Scanner_test() {
		List<String> ls = new ArrayList();
		ls.add("1 dika 39.10");
		ls.add("2 ahmad 40.00");

		for(String el : ls){
			Scanner in = new Scanner(el);
			StudentModel st = new StudentModel(in.nextInt(), in.next(), in.nextDouble());

			System.out.println(st.ToString());
		}
	}


	@Test
	void PriorityQueue() {

		int compareStr = "dika".compareTo("andika");

		PriorityQueue<StudentModel> pq = new PriorityQueue<StudentModel>((a,o) -> {
			if(a.getCGPA() < o.getCGPA()){
				return 1;
			}else if(a.getCGPA() > o.getCGPA()){
				return -1;
			}else{
				if(!a.getName().equals(o.getName())){
					return a.getName().compareTo(o.getName());
				}else{
					if(a.getCGPA() < o.getCGPA()){
						if(a.getID() < o.getID()){
							return -1;
						}else{
							return 1;
						}
					}else{
						return -1;
					}
				}
			}
		});

		// pq = new PriorityQueue<StudentModel>(Comparator.comparing(StudentModel::getCGPA).reversed().thenComparing(StudentModel::getName).thenComparing(StudentModel::getID));

		StudentModel data = new StudentModel(59,"dinda",35.78);
		pq.add(data);
		// System.out.println(data.ToString());

		data = new StudentModel(40,"andika",39.60);
		pq.add(data);
		// System.out.println(data.ToString());

		data = new StudentModel(34,"rama",31.80);
		pq.add(data);

		data = new StudentModel(21,"ahmad",29.98);
		pq.add(data);

		data = new StudentModel(24,"laras",37.98);
		pq.add(data);

		data = new StudentModel(23,"laras",37.98);
		pq.add(data);
		// System.out.println(data.ToString());

		// System.out.println("CABUTS -> "+pq.remove().ToString());

		// data = new StudentModel(13,"fadil",31.26);
		// pq.add(data);
		// System.out.println(data.ToString());

		// data = new StudentModel(20,"treisno",32.34);
		// pq.add(data);
		// System.out.println(data.ToString());

		data = new StudentModel(15,"lingga",12.99);
		pq.add(data);
		data = new StudentModel(18,"galing",12.99);
		pq.add(data);
		// System.out.println(data.ToString());

		// System.out.println("CABUTS -> "+pq.remove().ToString());

		// data = new StudentModel(34,"rama",31.80);
		// pq.add(data);
		// System.out.println(data.ToString());

		// System.out.println("CABUTS -> "+pq.remove().ToString());

		data = new StudentModel(37,"dimas",31.80);
		pq.add(data);
		// data = new StudentModel(44,"ahmad",39.80);
		// pq.add(data);
		// System.out.println(data.ToString());

		// System.out.println("============================ sisaan =========================");

		// for(StudentModel el : pq){
		// 	System.out.println(el.ToString());
		// }
		
		// List<StudentModel> list = new ArrayList<>(pq);

		// var dbg = "";

		while(!pq.isEmpty()){
			System.out.println(pq.remove().ToString());
		}

	}

	@Test
	public void regexTes(){
		String res = "{[]{}{}}}{".replaceAll("((\\{\\}|\\[\\]|\\(\\))|)", " ");

		String dbg = "";
	}

	@Test
	public void Stack_test(){
		String input = "(){}";
		Integer length = input.length();
		Deque<Character> deque = new ArrayDeque<Character>();
		boolean isValid = true;
		HashMap<Character,Integer> ops = new HashMap<Character,Integer>(){{
			put('[', 0);
			put('{', 1);
			put('(', 2);
		}};
		HashMap<Character,Integer> cls = new HashMap<Character,Integer>(){{
			put(']', 0);
			put('}', 1);
			put(')', 2);
		}};

		if(length % 2 == 0){
			
			deque = new ArrayDeque<>(input.chars().mapToObj(el -> (char) el).collect(Collectors.toList()));

			while(!deque.isEmpty()){
				char o = deque.removeFirst();
				char c = deque.removeLast();
				int ido = ops.get(o);
				int idc = cls.get(c);
				if(ido != idc){
					o = deque.removeFirst();
					c = deque.removeLast(); 
					if((ido != ops.get(o)) || (idc != cls.get(c))){
						isValid = false;
						break;
					}
				}
			}
			
		}
		else{
			isValid = false;
		}

		System.out.println(isValid);


	}

	@Test
	public void testIntLoop2(){
		Scanner in = new Scanner(System.in);
	}



}
