package com.AsyncDemo;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.Stack;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.AsyncDemo.Models.StudentModel;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import lombok.Getter;
import lombok.Setter;

@SpringBootTest
public class GenericTest {

	@Test
	public void TestGeneric(){
		MyData<ChildSuperClass> dataCovariant = new MyData<>(new ChildSuperClass());

		this.ProcessCovariant(dataCovariant);

		MyData<SuperClass> dataContravariant = new MyData<>(new SuperClass());

		this.ProcessContravariant(dataContravariant);

		//bahaya
		MyData<Object> dataContravariantBahaya = new MyData<>(new SuperClass());

		this.ProcessContravariant(dataContravariantBahaya);
	} 

	//boleh get data, bahaya untuk set data
	public void ProcessCovariant(MyData<? extends SuperClass> param){
		var res = param.getValue();
	}
	//set data aman, namun berbahaya saaat get data
	public void ProcessContravariant(MyData<? super SuperClass> param){
		var value = new ChildSuperClass();
		param.setValue(value);
	}

}

@Getter
@Setter
class MyData<T>{
	public MyData(){}
	public MyData(T val){
		this.Value = val;
	}
	private T Value;
}

@Getter
@Setter
class SuperClass{

	private String Name;
	private Integer Age;
	private String Grade;

	public String ToString(){

		ObjectMapper objMapper = new ObjectMapper();

		try {
			return objMapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return "";
	}
}

@Getter
@Setter
class ChildSuperClass extends SuperClass{

	public void Print(){
		String data = super.ToString();
		System.out.println(data);
	}

}

@Getter
@Setter
class BoundedType<T extends SuperClass>{

	private T Value;

	public void Print(){
		String data = this.Value.ToString();
		System.out.println(data);
	}

}
