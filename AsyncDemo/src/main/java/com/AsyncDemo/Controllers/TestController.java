package com.AsyncDemo.Controllers;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import com.AsyncDemo.Services.TestService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(value = "TestService")
public class TestController {

    @Autowired
    private TestService _testService;

    @RequestMapping(value = "Test", method = RequestMethod.POST, consumes = {MediaType.MULTIPART_FORM_DATA_VALUE},produces = MediaType.APPLICATION_JSON_VALUE)
    public CompletableFuture<List<Object>> Test(MultipartFile param) throws IOException {
        var res = this._testService.ReadCsv(param);
        return res;
    }

}
