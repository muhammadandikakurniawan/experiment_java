package com.AsyncDemo.Models;


import lombok.*;

@Getter
@Setter
public class StudentModel{
    
    private int ID;
    private String Name;
    private double CGPA;
    
    public StudentModel(){
        
    }
    public StudentModel(int id, String name, double cgpa){
        this.ID = id;
        this.Name = name;
        this.CGPA = cgpa;
    }

    public String ToString(){
        return String.format("%d - %s - %f", this.getID(), this.getName(), this.getCGPA());
    }

}
