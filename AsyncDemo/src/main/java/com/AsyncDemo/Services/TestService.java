package com.AsyncDemo.Services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class TestService {

    @Async
    public CompletableFuture<String> Method_test() {
        for (int i = 0; i < 100; i++) {

        }
        System.out.println(Thread.currentThread().getName());

        return CompletableFuture.completedFuture("ok");
    }

    @Async
    public CompletableFuture<List<Object>> ReadCsv(MultipartFile file) throws IOException {
        List<Object> res = new ArrayList<>();
        System.out.println(file.getOriginalFilename());
        try(final BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream()))){
            String row;

            while((row = br.readLine()) != null){
                System.out.println(row);
                List<String> col = Arrays.asList(row.split(";"));
                
                res.add(col);

            }
        }

        return CompletableFuture.completedFuture(res);
    }

}
